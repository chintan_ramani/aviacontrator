import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Message } from 'src/app/modal/message.model';
import { environment } from 'src/environments/environment';
import { Socket } from 'ngx-socket-io';

@Injectable({
	providedIn: 'root'
})

export class MessageService {
	constructor(
		private http: HttpClient,
		private socket: Socket
	) { }

	getUserList(): Observable<{ success: boolean; message: string, data: any }> {
		return this.http.get<{ success: boolean; message: string, data: any }>(environment.API_URL + "userlist");
	}

	getMessageListByUserId(params): Observable<{ success: boolean; message: string, data: any }> {
		return this.http.post<{ success: boolean; message: string, data: any }>(environment.API_URL + "getMessageByUserId", params);
	}

	userjoin(userId: number): void {
		this.socket.emit('join', userId);
	}

	sendMessage(message: Message): void {
		this.socket.emit('messagedetection', message);
	}

	receiverMessage(): Observable<any> {
		return Observable.create((observer) => {
			this.socket.on('message', (msg) => {
				observer.next(msg);
			});
		});
	}

	newUserJoin(): Observable<any> {
		return Observable.create(observer => {
			this.socket.on('userjoinedthechat', (msg) => {
				observer.next(msg);
			});
		});
	}

	userdisconnect(): Observable<any> {
		return Observable.create(observer => {
			this.socket.on('disconnect', userId => {
				observer.next(userId);
			});
		});
	}

	disconnect(userId): void {
		this.socket.disconnect();
	}

	connect(): void {
		this.socket.connect();
	}
}
