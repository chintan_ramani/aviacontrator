import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { APIResponse } from 'src/app/modal/apiresponse';
import { throwError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Injectable({
	providedIn: 'root'
})

export class JobService {

	constructor(private http: HttpClient, private toastr: ToastrService) { }
	getJobs(): Observable<APIResponse> {
		return this.http.get(environment.API_URL + 'getjobs').pipe(catchError(this.handleError));
	}

	postJob(form: any): Observable<APIResponse> {
		return this.http.post(environment.API_URL + 'postjob', form).pipe(catchError(this.handleError))
	}

	updateJob(form: any): Observable<APIResponse> {
		return this.http.post(environment.API_URL + 'updatejob', form).pipe(catchError(this.handleError))
	}

	applyjob(form: any): Observable<APIResponse> {
		return this.http.post(environment.API_URL + 'applyjob', form).pipe(catchError(this.handleError));
	}

	deleteJob(params: any): Observable<APIResponse> {
		return this.http.post(environment.API_URL + 'deletejob', params).pipe(catchError(this.handleError));
	}

	approveJob(params: any): Observable<APIResponse> {
		return this.http.post(environment.API_URL + 'approvejob', params).pipe(catchError(this.handleError));
	}

	disapproveJob(params: any): Observable<APIResponse> {
		return this.http.post(environment.API_URL + 'disapprovejob', params).pipe(catchError(this.handleError));
	}

	companyGetMyjob(): Observable<APIResponse> {
		return this.http.get(environment.API_URL + 'company/myjob').pipe(catchError(this.handleError));
	}

	contractorGetMyjob(): Observable<APIResponse> {
		return this.http.get(environment.API_URL + 'contractor/myjob').pipe(catchError(this.handleError));
	}

	getJobDetailsById(jobId): Observable<APIResponse> {
		return this.http.get(environment.API_URL + 'getjobdetailsbyid/' + jobId).pipe(catchError(this.handleError));
	}

	getPlanList(): Observable<APIResponse> {
		return this.http.get(environment.API_URL + 'getplanlist').pipe(catchError(this.handleError));
	}

	sendMessageContractor(params): Observable<APIResponse> {
		return this.http.post(environment.API_URL + 'sendmessagecontractor', params).pipe(catchError(this.handleError));
	}

	stripePayment(params): Observable<APIResponse> {
		return this.http.post(environment.API_URL + 'stripe', params).pipe(catchError(this.handleError));
	}

	setJobId(jobId:string):void {
		localStorage.setItem("jobId",jobId);
	}

	getJobId():number {
		return (localStorage.getItem("jobId") == null || localStorage.getItem("jobId") == undefined) ? 0 : Number(localStorage.getItem("jobId"));
	}

	removeJobId():void{
		localStorage.removeItem("jobId");
	}

	showSuccessMessage(message: string): void {
		this.toastr.success(message, "Success!");
	}

	showErrorMessage(message: string): void {
		this.toastr.error(message, "Error!");
	}

	handleError(error: HttpErrorResponse) {
		if (error.error instanceof ErrorEvent) {
			console.error(`An error occurred ${error.error.message}`);
		} else {
			console.error(`Backend returned code ${error.status},body was: ${error.error}`);
		}
		return throwError('Something bad happened; please try again later.');
	}

	splitArray(array: any, size: number):Array<number> {
		let newArray = [];
		for (let i = 0; i < array.length; i += size) {
			newArray.push(array.slice(i, i + size));
		}
		return newArray;
	}
}
