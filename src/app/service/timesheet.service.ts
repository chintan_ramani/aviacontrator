import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { APIResponse } from 'src/app/modal/apiresponse';
import { DatePipe } from '@angular/common';
import { throwError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Injectable({
	providedIn: 'root'
})

export class TimesheetService {

	applyJobCounter = new Subject<number>();
	approvedJobCounter = new Subject<number>();
	constructor(
		private http: HttpClient,
		private datePipe: DatePipe
	) { }

	getCompanyList(): Observable<APIResponse> {
		return this.http.get<APIResponse>(environment.API_URL + "getcompanylist").pipe(catchError(this.handleError));
	}

	addTimesheet(params): Observable<APIResponse> {
		return this.http.post<APIResponse>(environment.API_URL + "addtimesheet", params).pipe(catchError(this.handleError));
	}

	getTimesheet(params): Observable<APIResponse> {
		return this.http.post<APIResponse>(environment.API_URL + "gettimesheet", params).pipe(catchError(this.handleError));
	}

	getCompanyTimesheet(params): Observable<APIResponse> {
		return this.http.post<APIResponse>(environment.API_URL + "company/timesheet", params).pipe(catchError(this.handleError));
	}

	approveTimesheetHours(params): Observable<APIResponse> {
		return this.http.post<APIResponse>(environment.API_URL + "company/approveTimesheet", params).pipe(catchError(this.handleError));
	}

	updateTimeSheetHourById(params): Observable<APIResponse> {
		return this.http.post<APIResponse>(environment.API_URL + "company/updatetimesheet", params).pipe(catchError(this.handleError));
	}

	getTimesheetDetail(params): Observable<APIResponse> {
		return this.http.post<APIResponse>(environment.API_URL + "gettimesheetdetail", params).pipe(catchError(this.handleError));
	}

	getPaymentHistory(page): Observable<APIResponse> {
		return this.http.get<APIResponse>(environment.API_URL + "payment-history?page=" + page).pipe(catchError(this.handleError));
	}

	setApplyNotificationCouter(counter): void {
		this.applyJobCounter.next(counter);
	}

	setApprovedNotificationCouter(counter): void {
		this.approvedJobCounter.next(counter);
	}

	changeDateFormat(date): string {
		return this.datePipe.transform(date, "y-MM-dd");
	}

	changeTimeFormat(date): string {
		return this.datePipe.transform(date, "HH:mm");
	}

	decimalNumber(price): string {
		return Number(price).toFixed(2);
	}

	handleError(error: HttpErrorResponse) {
		if (error.error instanceof ErrorEvent) {
			console.error('An error occurred:', error.error.message);
		} else {
			console.error(
				`Backend returned code ${error.status}, ` +
				`body was: ${error.error}`);
		}
		return throwError('Something bad happened; please try again later.');
	}
}
