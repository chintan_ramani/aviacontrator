import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { APIResponse } from 'src/app/modal/apiresponse';
import { throwError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Injectable({
	providedIn: 'root'
})

export class UserService {
	constructor(private http: HttpClient, private toastr: ToastrService) { }

	register(form: any): Observable<APIResponse> {
		return this.http.post(environment.API_URL + "register", form).pipe(catchError(this.handleError));
	}

	login(form: any): Observable<APIResponse> {
		return this.http.post(environment.API_URL + "login", form).pipe(catchError(this.handleError));
	}

	changePassword(form: any): Observable<APIResponse> {
		return this.http.post(environment.API_URL + "changePassword", form).pipe(catchError(this.handleError));
	}

	checkEmailId(value: any): Observable<APIResponse> {
		let form = new FormData();
		form.append("email", value);
		return this.http.post(environment.API_URL + "checkEmailId", form).pipe(catchError(this.handleError));
	}

	checkEmailIdExist(value: any): Observable<APIResponse> {
		let form = new FormData();
		form.append("email", value);
		return this.http.post(environment.API_URL + "checkEmailIdExist", form).pipe(catchError(this.handleError));
	}

	userSelectedPlan(form: any): Observable<APIResponse> {
		return this.http.post(environment.API_URL + "userselectedplan", form).pipe(catchError(this.handleError));
	}

	contactUs(form: any): Observable<APIResponse> {
		return this.http.post(environment.API_URL + "contactus", form).pipe(catchError(this.handleError));
	}

	forgotPassword(form: any): Observable<APIResponse> {
		return this.http.post(environment.API_URL + "forgotpassword", form).pipe(catchError(this.handleError));
	}

	getUserDetail(): Observable<APIResponse> {
		return this.http.get(environment.API_URL + "getuserdetail").pipe(catchError(this.handleError));
	}

	fileUpload(file: FormData): Observable<APIResponse> {
		return this.http.post(environment.API_URL + "fileupload", file).pipe(catchError(this.handleError));
	}

	updateUserDetail(form: any): Observable<APIResponse> {
		return this.http.post(environment.API_URL + "updateuserdetail", form).pipe(catchError(this.handleError));
	}

	deleteDocument(params: any): Observable<APIResponse> {
		return this.http.post(environment.API_URL + 'deletedocument', params).pipe(catchError(this.handleError));
	}

	getAllUserLocation(): Observable<APIResponse> {
		return this.http.get(environment.API_URL + 'userlocation').pipe(catchError(this.handleError));
	}

	getProfileDetailsById(userId): Observable<APIResponse> {
		return this.http.get(environment.API_URL + 'getprofile/' + userId).pipe(catchError(this.handleError));
	}

	getJobNotificationCounter(): Observable<APIResponse> {
		return this.http.get(environment.API_URL + 'notificationcounter').pipe(catchError(this.handleError));
	}

	getApprovedJobCounter(): Observable<APIResponse> {
		return this.http.get(environment.API_URL + 'approvedjobcounter').pipe(catchError(this.handleError));
	}

	verifyToken(args): Observable<APIResponse> {
		return this.http.post(environment.API_URL + 'verifytoken', args).pipe(catchError(this.handleError));
	}

	saveAuthData(data): void {
		localStorage.setItem("token", data.token);
		localStorage.setItem("userType", data.userType);
		localStorage.setItem("id", data.id);
	}

	logout(): void {
		localStorage.removeItem("token");
		localStorage.removeItem("id");
		localStorage.removeItem("userType");
		localStorage.clear();
	}

	getUserid(): number {
		return localStorage.getItem("id") != undefined ? Number(localStorage.getItem("id")) : 0;
	}
	
	getUserType(): number {
		return localStorage.getItem("userType") != undefined ? Number(localStorage.getItem("userType")) : 0;
	}

	getToken(): string {
		return localStorage.getItem("token");
	}

	loggedIn(): boolean {
		return !!localStorage.getItem("id");
	}

	showSuccessMessage(message: string): void {
		this.toastr.success(message, "Success!");
	}

	showErrorMessage(message: string): void {
		this.toastr.error(message, "Error!");
	}

	handleError(error: HttpErrorResponse) {
		if (error.error instanceof ErrorEvent) {
			console.error(`An error occurred:', ${error.error.message}`);
		} else {
			console.error(`Backend returned code ${error.status},body was: ${error.error}`);
		}
		return throwError('Something bad happened; please try again later.');
	}
}
