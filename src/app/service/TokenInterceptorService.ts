import {
	HttpInterceptor,
	HttpRequest,
	HttpHandler,
	HttpErrorResponse,
} from '@angular/common/http';
import { finalize, catchError } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from './user.service';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { of } from 'rxjs';

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {
	constructor(
		private userService: UserService,
		private spinner: NgxSpinnerService,
		private router: Router
	) { }

	intercept(request: HttpRequest<any>, next: HttpHandler) {
		this.spinner.show();
		const authToken = this.userService.getToken();
		let setHeaders = { Accept: 'application/json' };
		if (authToken) {
			setHeaders = { ...setHeaders, ...{ Authorization: `Bearer ${authToken}` } };
		}
		return next.handle(request.clone({ setHeaders })).pipe(
			catchError((error: any) => {
				if (error instanceof HttpErrorResponse) {
					if (error.status === 0) {
						this.userService.showErrorMessage('Invalid Request');
					}
					else if (error.status === 401) {
						this.userService.showErrorMessage('Unauthorized');
						this.userService.logout();
						this.router.navigate(['']);
					}
					else if (error.status === 500) {
						this.userService.showErrorMessage('Something went wrong,Please try again later...');
					}
				}
				return of(error);
			}),
			finalize(() => {
				this.spinner.hide();
			}),
		);
	}
}
