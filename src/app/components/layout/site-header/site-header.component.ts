import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { UserService } from 'src/app/service/user.service';

@Component({
	selector: 'app-site-header',
	templateUrl: './site-header.component.html',
	styleUrls: ['./site-header.component.css']
})

export class SiteHeaderComponent implements OnInit {
	isFixed: boolean = false;
	logoURL: string = "assets/img/logo.svg";
	isLogin:boolean = false;

	constructor(private router: Router,private userService:UserService) { }

	ngOnInit() {
		this.isFixed = (this.router.url === "/") ? true : false;
		this.logoURL = `assets/img/${this.router.url==='/' ? 'logo.svg' : 'logo.png'}`;
		this.router.events.subscribe((event: any) => {
			if (event instanceof NavigationEnd) {
				this.isFixed = (event.url === "/") ? true : false;
				this.logoURL = `assets/img/${event.url==='/' ? 'logo.svg' : 'logo.png'}`;
			}
		});
		this.isLogin = this.userService.loggedIn();
	}

	closeNavbar(element: HTMLElement):void {
		element.classList.remove("show");
	}

	navigateToLogin(): void {
		this.router.navigateByUrl((this.userService.getUserType()==2  ? 'company/post-job' : 'contractor/jobs'));
	}
}
