import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorLayoutComponent } from './contractor-layout.component';

describe('ContractorLayoutComponent', () => {
  let component: ContractorLayoutComponent;
  let fixture: ComponentFixture<ContractorLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractorLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractorLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
