import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/service/user.service';
import { TimesheetService } from 'src/app/service/timesheet.service';

@Component({
	selector: 'app-company-sidebar',
	templateUrl: './company-sidebar.component.html',
	styleUrls: ['./company-sidebar.component.css']
})

export class CompanySidebarComponent implements OnInit {

	applyJobCounter: number = 0;
	constructor(
		private timesheetService: TimesheetService,
		private userService: UserService,
		private router: Router
	) { }

	ngOnInit() {

		this.timesheetService.applyJobCounter.subscribe(response=>{
			this.applyJobCounter = response;
		});
		this.userService.getJobNotificationCounter().subscribe(response=> {
			if(response.success) {
				this.applyJobCounter = response.data.apply_job_counter;
			}
			else {
				console.log(response,"Error");
			}
		},error=>{
			console.log("Error : ",error);
		});
	}
	logout(): void {
		this.userService.logout();
		this.userService.showSuccessMessage("Logout successfully");
		this.router.navigate(['/login']);
	}
}
