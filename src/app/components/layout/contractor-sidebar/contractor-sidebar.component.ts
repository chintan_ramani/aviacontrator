import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/service/user.service';
import { TimesheetService } from 'src/app/service/timesheet.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-contractor-sidebar',
	templateUrl: './contractor-sidebar.component.html',
	styleUrls: ['./contractor-sidebar.component.css']
})
export class ContractorSidebarComponent implements OnInit {

	approvedJobCounter:number = 0;
	constructor(
		private userService: UserService,
		private timesheetService:TimesheetService,
		private router: Router
	) { }

	ngOnInit() {
		this.timesheetService.approvedJobCounter.subscribe(response=>{
			this.approvedJobCounter = response;
		});
		this.userService.getApprovedJobCounter().subscribe(response=> {
			if(response.success) {
				this.approvedJobCounter = response.data.approved_job_counter;
			}
			else {
				console.log(response,"Error");
			}
		},error=>{
			console.log("Error : ",error);
		});
	}
	
	logout(): void {
		this.userService.logout();
		this.userService.showSuccessMessage("Successfully logout");
		this.router.navigate(['/login']);
	}
}
