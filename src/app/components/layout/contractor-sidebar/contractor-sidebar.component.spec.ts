import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorSidebarComponent } from './contractor-sidebar.component';
import { HttpClientTestingModule } from '@angular/common/http/testing'
describe('ContractorSidebarComponent', () => {
  let component: ContractorSidebarComponent;
  let fixture: ComponentFixture<ContractorSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractorSidebarComponent ],
      imports:[HttpClientTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractorSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
