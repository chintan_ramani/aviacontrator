import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/service/user.service';
import { Router } from '@angular/router';
import { OwlOptions } from 'ngx-owl-carousel-o';
declare const $;

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

	lat = 54.687157;
	lng = 25.279652;
	zoom = 8;
	userLocationList: any = [];
	jobLocationList: any = [];
	currentInfoWindowIndex = '0';
	markerUrl = {};
	title = 'AVIACONTRACTOR';
	// slider	: OwlOptions = {
	// 	loop: true,
	// 	margin: 10,
	// 	nav: false,
	// 	responsive: {
	// 		0: {
	// 			items: 1
	// 		},
	// 		600: {
	// 			items: 1
	// 		},
	// 		1000: {
	// 			items: 1
	// 		}
	// 	}
	//   };
	constructor(
		private router: Router,
		private userService: UserService
	) { }

	ngOnInit() {
		this.markerUrl = {
			url: 'assets/img/location-pin.svg',
			scaledSize: {
				width: 40,
				height: 60
			},
		};
		this.getAllUserLocation();
		$('.owl-carousel').owlCarousel({
			loop: true,
			margin: 10,
			nav: false,
			responsive: {
				0: {
					items: 1
				},
				600: {
					items: 1
				},
				1000: {
					items: 1
				}
			}
		});
	}
	
	getAllUserLocation():void {
		this.userService.getAllUserLocation().subscribe(response => {
			if (response.success) {
			//	this.userLocationList = response.data.userList;
				this.jobLocationList = response.data.jobList;
			}
			else {
				this.userService.showErrorMessage(response.message);
			}
		})
	}
	
	markerClick(index:string):void {
		this.currentInfoWindowIndex = index;
	}

	navigate(url, id, closeModal):void {
		closeModal.click();
		this.router.navigate([url, id]);
	}
}
