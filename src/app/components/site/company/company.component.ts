import { Component, OnInit } from '@angular/core';
import { FormValidatorService } from 'src/app/service/form-validator.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { UserService } from 'src/app/service/user.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-company',
	templateUrl: './company.component.html',
	styleUrls: ['./company.component.css']
})

export class CompanyComponent implements OnInit {
	registerForm: FormGroup;
	constructor(
		private router: Router,
		private formBuilder: FormBuilder,
		private userService:UserService,
		private formValidatorService:FormValidatorService
	) { }

	ngOnInit() {
		this.registerForm = this.formBuilder.group({
			companyName: ['', [Validators.required]],
			jobTitle: ['', [Validators.required]],
			firstName: ['', [Validators.required]],
			lastName: ['', [Validators.required]],
			email: ['', [Validators.required, Validators.email]],
			mobileNumber: ['', [Validators.required, Validators.pattern('[0-9]+')]],
			password: ['', Validators.required],
			confirmPassword: ['', Validators.required],
			userType: ['2']
		},{validator: this.checkPasswords });
	}

	checkPasswords(group: FormGroup) { 
		let password = group.get('password').value;
		let confirmPassword = group.get('confirmPassword').value;
		return  password === confirmPassword ? null : { mustMatch: true }; 
	}
	
	onSubmit() {		
		if(this.registerForm.valid){
			this.userService.register(this.registerForm.value).subscribe(response => {
					if (response.success) {
							this.userService.showSuccessMessage(response.message);
							this.router.navigate(["/company-login"]);
					}
					else {
							this.userService.showErrorMessage(response.message);
					}                                
			});                        
		}
		else {                     
				this.formValidatorService.validateAllFormFields(this.registerForm);
		}
	}
}
