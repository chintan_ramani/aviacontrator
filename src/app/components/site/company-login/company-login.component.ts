import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/service/user.service';
import { FormValidatorService } from 'src/app/service/form-validator.service';
import { JobService } from 'src/app/service/job.service';

@Component({
        selector: 'app-company-login',
        templateUrl: './company-login.component.html',
        styleUrls: ['./company-login.component.css']
})

export class CompanyLoginComponent implements OnInit {
        loginForm: FormGroup;
        constructor(
                private router: Router,
                private formBuilder: FormBuilder,
                private userService: UserService,
                private jobService: JobService,
                private formValidatorService: FormValidatorService
        ) { }

        ngOnInit():void {
                this.loginForm = this.formBuilder.group({
                        email: ['', [Validators.required, Validators.email]],
                        password: ['', Validators.required],
                        userType: ['2']
                });
        }

        onSubmit():void {
                if (this.loginForm.valid) {
                        this.userService.login(this.loginForm.value).subscribe(response => {
                                if (response.success) {
                                        this.userService.showSuccessMessage(response.message);
                                        this.userService.saveAuthData(response.data);
                                        this.jobService.removeJobId();
                                        this.router.navigate(["/company/post-job"]);
                                }
                                else {
                                        this.userService.showErrorMessage(response.message);
                                }
                        });
                } else {
                        this.formValidatorService.validateAllFormFields(this.loginForm);
                }
        }
}
