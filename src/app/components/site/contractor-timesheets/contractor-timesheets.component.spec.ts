import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorTimesheetsComponent } from './contractor-timesheets.component';

describe('ContractorTimesheetsComponent', () => {
  let component: ContractorTimesheetsComponent;
  let fixture: ComponentFixture<ContractorTimesheetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractorTimesheetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractorTimesheetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
