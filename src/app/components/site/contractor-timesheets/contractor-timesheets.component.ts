import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, FormArray } from "@angular/forms";
import { ModalManager } from "ngb-modal";
import { TimesheetService } from "src/app/service/timesheet.service";
import { UserService } from "src/app/service/user.service";

@Component({
	selector: "app-contractor-timesheets",
	templateUrl: "./contractor-timesheets.component.html",
	styleUrls: ["./contractor-timesheets.component.css"],
})
export class ContractorTimesheetsComponent implements OnInit {
	private modalRef;
	totalHour = 0;
	datesList: any = [];
	timesheetList: any[] = [];
	selectedDate: Date;
	currentDate: Date;
	fromTime: Date;
	toTime: Date;
	startDate = "";
	endDate = "";
	isFirstTime = true;
	isReadOnly = true;
	ismeridian = false;
	submitted = false;
	selectedTimesheetId = 0;
	addTimesheetForm: FormGroup;
	@ViewChild("myModal") myModal;
	constructor(
		private modalService: ModalManager,
		private formBuilder: FormBuilder,
		private timesheetService: TimesheetService,
		private userService: UserService
	) { }

	ngOnInit(): void {
		this.fromTime = new Date(new Date().setHours(new Date().getHours() - 8));
		this.toTime = new Date();
		this.addTimesheetForm = this.formBuilder.group({
			timesheet: this.formBuilder.array([]),
		});
		this.currentDate = new Date();
		this.setDateList(this.timesheetService.changeDateFormat(this.currentDate));
		this.addTimesheet();
	}

	get timesheet(): FormArray {
		return this.addTimesheetForm.controls.timesheet as FormArray;
	}

	addTimesheet(): void {
		this.timesheet.push(this.addTimesheetFormGroup());
	}

	addTimesheetFormGroup(): FormGroup {
		return this.formBuilder.group({
			startTime: [this.fromTime],
			endTime: [this.toTime],
		});
	}

	removeTimesheet(index): void {
		this.timesheet.removeAt(index);
	}

	setDateList(date): void {
		this.datesList = [];
		for (let i = 0; i < 7; i++) {
			this.datesList.push(
				this.timesheetService.changeDateFormat(
					new Date(new Date(date).setDate(new Date(date).getDate() + i))
				)
			);
		}
		this.startDate = this.datesList[0];
		this.endDate = this.datesList[6];
		const params = { dates: this.datesList };
		this.timesheetService.getTimesheet(params).subscribe((response) => {
			if (response.success) {
				this.timesheetList = response.data;
			} else {
				this.userService.showErrorMessage(response.message);
			}
		});
	}

	onChangeDate(type): void {
		const firstDay = new Date(this.startDate);
		const date =
			type === 1
				? new Date(firstDay.getTime() + 604800000)
				: new Date(firstDay.getTime() - 604800000);
		this.setDateList(this.timesheetService.changeDateFormat(date));
	}

	openModal(data): void {
		this.submitted = false;
		this.selectedTimesheetId = data.id;
		this.selectedDate = data.date;
		this.timesheet.clear();
		if (data.id !== 0 && Array.isArray(data.timesheet)) {
			data.timesheet.forEach((element) => {
				let time = element.startTime.split(":");
				const date = new Date();
				date.setHours(time[0]);
				date.setMinutes(time[1]);
				this.fromTime = new Date(date);
				time = element.endTime.split(":");
				date.setHours(time[0]);
				date.setMinutes(time[1]);
				this.toTime = new Date(date);
				this.addTimesheet();
			});
		} else {
			this.addTimesheet();
		}
		this.modalRef = this.modalService.open(this.myModal,{backdrop: 'static'});
	}

	onSubmit(): void {
		this.submitted = true;
		if (this.addTimesheetForm.invalid) {
			return;
		}
		const form = new FormData();
		form.append("date", this.selectedDate.toString());
		form.append("id", this.selectedTimesheetId.toString());
		this.timesheet.controls.forEach((element, index) => {
			form.append(`startTime[${index}]`, this.timesheetService.changeTimeFormat(element.value.startTime));
			form.append(`endTime[${index}]`, this.timesheetService.changeTimeFormat(element.value.endTime));
		});
		this.timesheetService.addTimesheet(form).subscribe((response) => {
			if (response.success) {
				this.modalService.close(this.modalRef);
				this.userService.showSuccessMessage(response.message);
				this.setDateList(this.startDate);
			} else {
				this.userService.showErrorMessage(response.message);
			}
		});
	}

	checkDate(date): boolean {
		return new Date(date) < new Date();
	}
}
