import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorJobComponent } from './contractor-job.component';

describe('ContractorJobComponent', () => {
  let component: ContractorJobComponent;
  let fixture: ComponentFixture<ContractorJobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractorJobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractorJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
