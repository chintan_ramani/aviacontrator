import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormValidatorService } from 'src/app/service/form-validator.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/service/user.service';
import { JobService } from 'src/app/service/job.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { AgmMap } from '@agm/core';

@Component({
	selector: 'app-contractor-job',
	templateUrl: './contractor-job.component.html',
	styleUrls: ['./contractor-job.component.css']
})

export class ContractorJobComponent implements OnInit {
	protected map: any;
	@ViewChild(AgmMap) public agmMap: AgmMap
	@ViewChild('closeModel') closeModel: ElementRef;
	zoom = 8;
	markerUrl = {};
	lat = 54.687157;
	lng = 25.279652;
	showMap = false;
	jobArray: any[] = [];
	showAlertBox = true;
	searchText: string = "";
	applyForm: FormGroup;
	popupJobIndex: number = 0;
	currentInfoWindowIndex = 0;
	jobLocationList: any = [];
	jobId = '0';
	constructor(
		private jobservice: JobService,
		private formBulder: FormBuilder,
		private router: Router,
		private route: ActivatedRoute,
		private formValidationservice: FormValidatorService
	) { }

	ngOnInit(): void {
		this.route.queryParams.subscribe(({jobId})=> {
			this.jobId = jobId;
		});
		this.markerUrl = {
			url: 'assets/img/location-pin.svg',
			scaledSize: {
				width: 40,
				height: 60
			},
		};
		this.applyForm = this.formBulder.group({
			jobTermCondtion: [false, Validators.requiredTrue],
			jobDescription: ['', Validators.required],
			jobId: []
		});
		this.getJobs();
	}

	mapReady(map):void {
		this.map = map;
	}

	getJobs(): void {
		this.jobservice.getJobs().subscribe((response) => {
			if (response.success) {
				this.jobArray = response.data.joblist;
				this.jobLocationList = response.data.joblist;
				this.showAlertBox = !response.data.isProfileCompleted;
				if(this.jobId !== "0") {
					setTimeout( () => {
					 	if((<HTMLDivElement>document.querySelector(`#jobSection${this.jobId}`))) {
							(<HTMLDivElement>document.querySelector(`#jobSection${this.jobId}`)).click();
						}
					},1000);
				}
			}	
			else {
				this.jobservice.showErrorMessage(response.message);
			}
		}, (error) => {
			console.log(`Get Api Error ${error}`);
		});
	}

	onJobClick(index: number): void {
		this.applyForm.patchValue({ jobDescription: '', jobId: this.jobArray[index].id });
		this.popupJobIndex = index;
	}

	onJobApplySubmit(): void {
		if (this.applyForm.valid) {
			this.jobservice.applyjob(this.applyForm.value).subscribe((response) => {
				if (response.success) {
					this.closeModel.nativeElement.click();
					this.ngOnInit();
					this.jobservice.showSuccessMessage(response.message);
				}
				else {
					this.jobservice.showErrorMessage(response.message);
				}
			}, (error) => {
				console.log(`Job apply api error ${error}`);
			});
		}
		else {
			this.formValidationservice.validateAllFormFields(this.applyForm);
		}
	}

	markerClick(index: number): void {
		this.currentInfoWindowIndex = index;
	}

	navigate(url: string, id: number, closeModal: HTMLButtonElement): void {
		closeModal.click();
		this.router.navigate([url, id]);
		this.showMap = true;
	}

	onSelectJob(jobId: number, lat: number, lng: number): void {
		this.agmMap.triggerResize();
		this.map.setCenter({ lat: Number(lat), lng: Number(lng) });
		this.showMap = true;
		this.currentInfoWindowIndex = jobId;
	}
}
