import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { FormValidatorService } from 'src/app/service/form-validator.service';
import { UserService } from 'src/app/service/user.service';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';

@Component({
	selector: 'app-company-profile',
	templateUrl: './company-profile.component.html',
	styleUrls: ['./company-profile.component.css']
})

export class CompanyProfileComponent implements OnInit {

	isAddressChange:number = 0;
	latitude: string = "0";
	longitude: string = "0";
	profileForm: FormGroup;
	showImage: boolean = false;
	fileTypes: Array<string> = ['jpg', 'jpeg', 'png'];
	userImage;
	@ViewChild('image') image: ElementRef;
	@ViewChild('file') file: ElementRef;
	@ViewChild("loader") loader: ElementRef;
	@ViewChild('placesRef', { static: false }) placesRef: GooglePlaceDirective;
	
	constructor(
		private formBuilder: FormBuilder,
		private formValidatorService: FormValidatorService,
		private userService: UserService
	) { }

	ngOnInit():void {
		this.profileForm = this.formBuilder.group({
			firstName: ['', [Validators.required]],
			lastName: ['', [Validators.required]],
			jobTitle: ['', [Validators.required]],
			businessName: ['', [Validators.required]],
			companyName: ['', [Validators.required]],
			address: ['', [Validators.required]],
			email: ['', [Validators.required, Validators.email]],
			mobileNumber: ['', [Validators.required, Validators.pattern('[0-9]+')]],
			userType: [2]
		});
		this.getUserDetail();
	}

	getUserDetail():void {
		this.userService.getUserDetail().subscribe(response => {
			if (response.success) {
				this.profileForm.patchValue({
					firstName : response.data.firstName,
					lastName : response.data.lastName,
					email : response.data.email,
					companyName : response.data.companyName,
					businessName : response.data.businessName,
					jobTitle : response.data.jobTitle,
					address : response.data.address,
					mobileNumber : response.data.mobileNumber					
				});
				this.latitude = response.data.latitude;
				this.longitude = response.data.longitude;
				if (response.data.image) {
					this.userImage = response.data.image;
					this.showImage = true;
				}
			}
			else {
				this.userService.showErrorMessage(response.message);
			}
		});
	}

	openFileManager():void {
		this.file.nativeElement.click();
	}

	onSelectImage(files: any[]):void {
		if (files.length === 0) return;
		var extension = files[0].name.split('.').pop().toLowerCase();
		var isSuccess = this.fileTypes.indexOf(extension) > -1;
		if (isSuccess) {
			let reader = new FileReader();
			reader.readAsDataURL(files[0]);
			reader.onload = (_event) => {
				this.showImage = true;
				this.userImage = reader.result;
				this.fileUpload(files[0]);
			}
		}
		else {
			this.userService.showErrorMessage("Please select only jpg,jpeg,png file");
		}
	}

	fileUpload(file: File):void {
		const form = new FormData();
		form.append('image', file);
		this.userService.fileUpload(form).subscribe(response => {
			if (!response.success) {
				this.userService.showErrorMessage(response.message);
			}
		});
	}
	onSubmit():void {
		if (this.profileForm.valid) {
			if ((this.latitude == "" && this.longitude == "") || (this.latitude == "0" && this.longitude == "0") || this.isAddressChange == 1) {
				this.userService.showErrorMessage("Please select address");
				return;
			}
			const params = {...this.profileForm.value,...{longitude: this.longitude, latitude: this.latitude } };
			this.userService.updateUserDetail(params).subscribe(response => {
				if (response.success) {
					this.userService.showSuccessMessage(response.message);
				}
				else {
					this.userService.showErrorMessage(response.message);
				}
			});
		}
		else {
			this.formValidatorService.validateAllFormFields(this.profileForm);
		}
	}

	changeAddress():void {		
		this.isAddressChange = 1;    
	}
 
	onChangeAddress(address: any): void {
		this.latitude = address.geometry.location.lat();
		this.longitude = address.geometry.location.lng();
		this.profileForm.patchValue({ address: address.formatted_address });
		this.isAddressChange = 0;
	}
}
