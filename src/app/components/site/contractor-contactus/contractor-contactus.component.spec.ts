import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorContactusComponent } from './contractor-contactus.component';

describe('ContractorContactusComponent', () => {
  let component: ContractorContactusComponent;
  let fixture: ComponentFixture<ContractorContactusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractorContactusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractorContactusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
