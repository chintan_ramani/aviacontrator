import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { UserService } from 'src/app/service/user.service';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})

export class ProfileComponent implements OnInit {

  
	imagePreview: string = "";
	userId: string = "";
	errorMessage: string = "";
	userDetails = {
		companyName:"",
		businessName:"",
		image:"",
		address:"",
		jobTitle:"",
		hourrate:"",
		companyCode:"",
		countryResidence:"",
		name:""
	}
	constructor(
		private spinner:NgxSpinnerService,
		private userService: UserService,
		private route: ActivatedRoute,
	) { }

	ngOnInit(): void {
		this.route.paramMap.subscribe((paramMap: ParamMap) => {
			this.userId = paramMap.get('userId');
			this.getProfileDetailsById();
		});
	}
	getProfileDetailsById() {
		this.spinner.show();
		this.userService.getProfileDetailsById(this.userId).subscribe(response => {
			if (response.success) {
				this.userDetails.image = response.data.image;
				this.userDetails.address = response.data.address;
				this.userDetails.businessName = response.data.businessName;
				this.userDetails.companyCode = response.data.companyCode;
				this.userDetails.companyName = response.data.companyName;
				this.userDetails.countryResidence = response.data.countryResidence;
				this.userDetails.hourrate = response.data.hourrate;
				this.userDetails.image = response.data.image;
				this.userDetails.jobTitle = response.data.jobTitle;
				this.userDetails.name = response.data.name;
				this.spinner.hide();
			}
			else {
				this.spinner.hide();
				this.userService.showErrorMessage(response.message);
			}
		}, error => {
			this.spinner.hide();
			this.userService.showErrorMessage("Something went wrong,Please try again later...");
		});
	}
}
