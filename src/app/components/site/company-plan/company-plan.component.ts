import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/service/user.service';
import { JobService } from 'src/app/service/job.service';
import { ModalManager } from 'ngb-modal';

@Component({
	selector: 'app-company-plan',
	templateUrl: './company-plan.component.html',
	styleUrls: ['./company-plan.component.css']
})

export class CompanyPlanComponent implements OnInit {
	@ViewChild('paymentModal') paymentModal: ElementRef;
	private paymentModalRef;
	planList: any[] = [];
	isActivePlan = 0;
	selectedPlanId = 0;
	paymentError = '';
	paymentErrorMessage = '';
	stripeForm: FormGroup;
	freePlanSelected = false;
	previousPlanExpiry = false;
	stripeFormSubmited = false;

	constructor(
		private jobService: JobService,
		private userService: UserService,
		private modalService: ModalManager,
		private formBuilder: FormBuilder,
	) { }

	ngOnInit(): void {
		this.stripeForm = this.formBuilder.group({
			cardNumber: ['', [Validators.required, Validators.pattern('[0-9]+')]],
			cardMonth: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(2)]],
			cardYear: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(4), Validators.pattern('[0-9]+')]],
			cardCVV: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(4), Validators.pattern('[0-9]+')]],
		});
		this.getPlanList();
	}

	getPlanList(): void {
		this.jobService.getPlanList().subscribe((response) => {
			if (response.success) {
				this.previousPlanExpiry = response.data.previousPlanExpiry;
				this.freePlanSelected = response.data.freePlanSelected;
				response.data.planList.forEach(element => {
					if (element.isActivePlan === 1) {
						this.isActivePlan = 1;
					}
				});
				this.planList = response.data.planList;
			}
			else {
				this.jobService.showErrorMessage(response.message);
			}
		}, (error) => {
			console.log(`Get Plan List Api Error ${error}`);
		});
	}

	openPaymentModel(planId: number): void {
		this.stripeFormSubmited = false;
		this.stripeForm.reset();
		this.selectedPlanId = planId;
		this.paymentError = '';
		this.paymentModalRef = this.modalService.open(this.paymentModal, { size: 'md', backdrop: 'static' });
	}


	onSelectFreePlan(jobPlanId: string, freePlanSelected: boolean): void {
		if (!freePlanSelected) {
			this.userService.userSelectedPlan({ jobPlanId }).subscribe((response) => {
				if (response.success) {
					this.userService.showSuccessMessage(response.message);
					this.getPlanList();
				} else {
					this.userService.showErrorMessage(response.message);
				}
			}, (error) => {
				console.log(`Get Free Plan Api Error ${error}`);
			});
		}
	}
	stripePayment(stripeToken: string): void {
		this.jobService.stripePayment({ stripeToken, selectedPlanId: this.selectedPlanId }).subscribe((response) => {
			if (response) {
				this.modalService.close(this.paymentModalRef);
				this.userService.showSuccessMessage(response.message);
				document.querySelector("#message").innerHTML = '';
				this.getPlanList();
			}
			else {
				document.querySelector("#message").innerHTML = response.message;
				//this.paymentError = response.message;
			}
		}, (error) => {
			console.log("Stripe Payment Api Error", error);
		});
	}
	loadStripe(): void {
		if (!window.document.getElementById('stripe-script')) {
			var s = window.document.createElement("script");
			s.id = "stripe-script";
			s.type = "text/javascript";
			s.src = "https://checkout.stripe.com/checkout.js";
			window.document.body.appendChild(s);
		}
	}

	getToken(): void {
		this.stripeFormSubmited = true;
		if (this.stripeForm.invalid) {
			return;
		}
		this.paymentErrorMessage = '';
		document.querySelector("#message").innerHTML = '';
		(<any>window).Stripe.card.createToken({
			number: this.stripeForm.value.cardNumber,
			exp_month: this.stripeForm.value.cardMonth,
			exp_year: this.stripeForm.value.cardYear,
			cvc: this.stripeForm.value.cardCVV
		}, (status: number, response: any) => {
			if (status == 200) {
				this.paymentErrorMessage = '';
				document.querySelector("#message").innerHTML = '';
				console.log(response, "Repsonse");
				this.stripePayment(response.id);
			}
			else {
				console.log(response.error.message);
				document.querySelector("#message").innerHTML = response.error.message;
				this.paymentErrorMessage = response.error.message;
				console.log(this.paymentErrorMessage);
			}
		});
	}
}
