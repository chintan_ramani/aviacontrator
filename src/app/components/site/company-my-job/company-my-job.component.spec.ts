import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyMyJobComponent } from './company-my-job.component';

describe('CompanyMyJobComponent', () => {
  let component: CompanyMyJobComponent;
  let fixture: ComponentFixture<CompanyMyJobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyMyJobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyMyJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
