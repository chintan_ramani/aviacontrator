import { FormGroup, FormArray, Validators, FormBuilder, FormControl, ValidationErrors } from '@angular/forms';
import { FormValidatorService } from 'src/app/service/form-validator.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { TimesheetService } from 'src/app/service/timesheet.service';
import { JobService } from 'src/app/service/job.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';

@Component({
	selector: 'app-company-my-job',
	templateUrl: './company-my-job.component.html',
	styleUrls: ['./company-my-job.component.css']
})

export class CompanyMyJobComponent implements OnInit {

	postjobform: FormGroup;
	currentJobs: any = [];
	applications: any = [];
	complateJobs: any = [];
	upcomingJobs: any = [];
	documentArray: any = [];
	selecteduserid: number = 0;
	isAddressChange: number = 1;
	latitude: string = "0";
	longitude: string = "0";
	description: string = "";
	min: Date = new Date();
	minEndDate: Date = new Date();
	@ViewChild("placesRef") placesRef : GooglePlaceDirective;
	@ViewChild('closeModel') closeModel: ElementRef;
	pattern: string = "^[0-9]+(.[0-9]{1,2})?$";
	currentJobCount = 0;
	upcomingJobCount = 0;
	completeJobCount = 0;

	constructor(
		private router: Router,
		private jobservice: JobService,
		private formBuilder: FormBuilder,
		private timesheetService: TimesheetService,
		private formValidatorService: FormValidatorService,
	) { }

	ngOnInit(): void {
		this.min.setDate(this.min.getDate() - 1);
		this.minEndDate.setDate(this.minEndDate.getDate() - 1);
		this.timesheetService.setApplyNotificationCouter(0);
		this.postjobform = this.formBuilder.group({
			position: ['', Validators.required],
			location: ['', Validators.required],
			jobDescription: ['', Validators.required],
			siftRosterOn: [false],
			siftRosterOnTime: [{ value: '', disabled: true }, Validators.pattern(this.pattern)],
			siftRosterOff: [false],
			siftRosterOffTime: [{ value: '', disabled: true }, Validators.pattern(this.pattern)],
			hourPerDay: ['', [Validators.required, Validators.pattern(this.pattern)]],
			dayShift: [false],
			nightShift: [false],
			bothShift: [false],
			rate: ['', [Validators.required, Validators.pattern(this.pattern)]],
			contractStart: ['', Validators.required],
			contractEnd: ['', Validators.required],
			cv: [false],
			license: [false],
			reference: [false],
			hf: [false],
			cdccl: [false],
			ewis: [false],
			other: [false],
			id: [0],
			otherText: this.formBuilder.array([]),
			type: ['hourly'],
		});
		this.postjobform.get('siftRosterOn').valueChanges.subscribe(change => {
			if (change) {
				this.postjobform.get('siftRosterOnTime').setValidators([Validators.pattern(this.pattern), Validators.required]);
				this.postjobform.get('siftRosterOnTime').enable();
			} else {
				this.postjobform.get('siftRosterOnTime').setValidators(null);
				this.postjobform.get('siftRosterOnTime').disable();
				this.postjobform.patchValue({ siftRosterOnTime: '' });
			}
			this.postjobform.get('siftRosterOnTime').updateValueAndValidity();
		});
		this.postjobform.get('siftRosterOff').valueChanges.subscribe(change => {
			if (change) {
				this.postjobform.get('siftRosterOffTime').setValidators([Validators.pattern(this.pattern), Validators.required]);
				this.postjobform.get('siftRosterOffTime').enable();
			} else {
				this.postjobform.get('siftRosterOffTime').setValidators(null);
				this.postjobform.get('siftRosterOffTime').disable();
				this.postjobform.patchValue({ siftRosterOffTime: '' });
			}
			this.postjobform.get('siftRosterOffTime').updateValueAndValidity();
		});
		this.postjobform.get('bothShift').valueChanges.subscribe(change => {
			let isChange = change ? true : false;
			this.postjobform.patchValue({
				dayShift: isChange,
				nightShift: isChange
			});
		});
		this.getMyjobs();
	}

	getMyjobs(): void {
		this.jobservice.companyGetMyjob().subscribe((response) => {
			if (response.success) {
				this.currentJobCount = response.data.current_count || 0;
				this.upcomingJobCount = response.data.upcoming_count || 0;
				this.completeJobCount = response.data.complate_count || 0;
				this.upcomingJobs = response.data.upcoming || [];
				this.currentJobs = response.data.current || [];
				this.complateJobs = response.data.complate || [];		
			}
			else {
				this.jobservice.showErrorMessage(response.message);
			}
		}, (error) => {
			console.log(`Get my job api ${error}`);
		});
	}

	// countJobs(jobs: any): number {
	// 	let count = 0;
	// 	jobs.forEach((element) => {
	// 		count += element.jobs.length;
	// 	});
	// 	return count;
	// }

	deleteJob(id: number): void {
		Swal.fire({
			title: 'Are you sure?',
			text: 'You will not be able to recover this job!',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Yes',
			cancelButtonText: 'No'
		}).then((result) => {
			if (result.value) {
				this.jobservice.deleteJob({ id }).subscribe((response) => {
					if (response.success) {
						this.ngOnInit();
						Swal.fire('Deleted!', response.message, 'success');
					}
					else {
						this.jobservice.showErrorMessage(response.message);
					}
				}, (error) => {
					console.log(`Delete job api ${error}`);
				});
			}
		});
	}

	approveJob(id: number): void {
		Swal.fire({
			title: 'Are you sure?',
			text: 'You approve this application!',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Yes',
			cancelButtonText: 'No'
		}).then((result) => {
			if (result.value) {
				this.jobservice.approveJob({ id }).subscribe((response) => {
					if (response.success) {
						this.ngOnInit();
						Swal.fire('Approved!', response.message, 'success');
					}
					else {
						this.jobservice.showErrorMessage(response.message);
					}
				}, (error) => {
					console.log(`Approved job api ${error}`);
				});
			}
		});
	}

	disApproveJob(id: number): void {
		Swal.fire({
			title: 'Are you sure?',
			text: 'You disapprove this application!',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Yes',
			cancelButtonText: 'No'
		}).then((result) => {
			if (result.value) {
				this.jobservice.disapproveJob({ id }).subscribe(response => {
					if (response.success) {
						this.ngOnInit();
						Swal.fire('Approved!', response.message, 'success');
					}
					else {
						this.jobservice.showErrorMessage(response.message);
					}
				}, (error) => {
					console.log(`Disapproved job api ${error}`);
				});
			}
		});
	}
	checkCV(list: any): void {
		this.selecteduserid = list.userId;
		this.description = list.description;
		this.documentArray = list;
	}
	onEditJob(job: any): void {		
		this.otherTextList.clear();	
		this.postjobform.patchValue({
			id: job.id,
			position: job.position,
			location: job.location,
			jobDescription: job.jobDescription,
			siftRosterOn: job.siftRosterOn,
			siftRosterOff: job.siftRosterOff,
			siftRosterOnTime: job.siftRosterOn ? job.siftRosterOnTime : '',
			siftRosterOffTime: job.siftRosterOff ? job.siftRosterOffTime : '',
			hourPerDay: job.hourPerDay,
			dayShift: job.dayShift,
			nightShift: job.nightShift,
			rate: job.rate,
			contractStart: job.contractStart,
			contractEnd: job.contractEnd,
			cv: job.cv,
			license: job.license,
			reference: job.reference,
			hf: job.hf,
			cdccl: job.cdccl,
			ewis: job.ewis,
			other: job.other,
			type: job.type,
		});
		if (Array.isArray(job.otherText)) {
			job.otherText.forEach(element => {
				this.addOtherText(element);
			});
		}
		this.isAddressChange = 0;
		this.latitude = job.latitude;
		this.longitude = job.longitude;
	}

	get otherTextList(): FormArray {
		return this.postjobform.controls.otherText as FormArray;
	}

	addOtherText(value = ''): void {
		this.otherTextList.push(new FormControl(value));
	}

	removeOtherText(index: number): void {
		this.otherTextList.removeAt(index);
	}

	onUpdate(): void {
		if (this.postjobform.valid) {
			if ((this.latitude == "" && this.longitude == "") || (this.latitude == "0" && this.longitude == "0") || this.isAddressChange == 1) {
				console.log("IN valid");
				this.jobservice.showErrorMessage("Please select location");
				return;
			}
			this.postjobform.patchValue({
				contractStart: new DatePipe('en-US').transform(this.postjobform.get('contractStart').value, "y-M-d"),
				contractEnd: new DatePipe('en-US').transform(this.postjobform.get('contractEnd').value, "y-M-d")
			});
			this.closeModel.nativeElement.click();
			const params = { ...{ latitude: this.latitude, longitude: this.longitude }, ...this.postjobform.value };
			this.jobservice.updateJob(params).subscribe(response => {
				if (response.success) {
					this.ngOnInit();
					this.jobservice.showSuccessMessage(response.message);
				}
				else {
					this.jobservice.showErrorMessage(response.message);
				}
			}, (error) => {
				console.log("IN 1valid");
				console.log(`Update job api ${error}`);
			});
		}
		else {
			Object.keys(this.postjobform.controls).forEach(key => {
				const controlErrors: ValidationErrors = this.postjobform.get(key).errors;
				if (controlErrors != null) {
					  Object.keys(controlErrors).forEach(keyError => {
						console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
					  });
					}
				  });
			console.log("IN 1vasdasdalid");
			this.formValidatorService.validateAllFormFields(this.postjobform);
		}
	}

	changeAddress(): void {
		this.isAddressChange = 1;
	}

	onChangeAddress(address: any): void {
		this.latitude = address.geometry.location.lat();
		this.longitude = address.geometry.location.lng();
		this.postjobform.patchValue({ location: address.formatted_address });
		this.isAddressChange = 0;
	}

	sendMessageContractor(): void {
		this.jobservice.sendMessageContractor({ user_id: this.selecteduserid }).subscribe(response => {
			if (response.success) {
				(<HTMLButtonElement>document.getElementById("cv-modal-close")).click();
				this.router.navigate(["/company/chat"]);
			}
			else {
				this.jobservice.showErrorMessage(response.message);
			}
		}, (error) => {
			console.log(`Send message to contractor api ${error}`);
		});
	}
}
