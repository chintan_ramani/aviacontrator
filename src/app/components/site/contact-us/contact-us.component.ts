import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { UserService } from 'src/app/service/user.service';
import { FormValidatorService } from 'src/app/service/form-validator.service';

@Component({
	selector: 'app-contact-us',
	templateUrl: './contact-us.component.html',
	styleUrls: ['./contact-us.component.css']
})

export class ContactUsComponent implements OnInit {
	contactForm: FormGroup;
	constructor(
                private formBuilder: FormBuilder, 
                private userService: UserService,
                private formValidatorService: FormValidatorService
        ) {}

	ngOnInit() {
		this.contactForm = this.formBuilder.group({
			name: ['', Validators.required],
			email: ['', [Validators.required, Validators.email]],
			message: ['', Validators.required]
		});
	}
	onSubmit() {
                if(this.contactForm.valid){
                        this.userService.contactUs(this.contactForm.value).subscribe(response=>{
                                if(response.success){
                                        this.userService.showSuccessMessage(response.message);
                                        this.contactForm.reset();
                                }
                                else {
                                        this.userService.showErrorMessage(response.message);
                                }                                
                        });
                }
                else{
                        this.formValidatorService.validateAllFormFields(this.contactForm);
                }
        }
}
