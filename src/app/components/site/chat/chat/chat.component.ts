import { Component, OnInit } from '@angular/core';
import { MessageService } from 'src/app/service/message.service';
import { UserService } from '../../../../service/user.service';
import { Message } from '../../../../modal/message.model';
import { ActivatedRoute,ParamMap,Router } from '@angular/router';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})

export class ChatComponent implements OnInit {

	messages = [];
	defaultSelectedUser:number = 0;
	selectedUserId: number;
	selectedUserName: number;
	selectedUser:object={};
	userId: number;
	jobId:number;
	postId:number;
	showContactList:boolean  = false;
	showMessageList:boolean  = false;
	
	constructor(
		private messageService: MessageService, 
		private userService: UserService,
		private route: ActivatedRoute,
		private router:Router
	){ }

	ngOnInit(): void {

	
		this.messageService.connect();
		this.userId = Number(this.userService.getUserid());
		this.userjoin();
		this.messageService.receiverMessage().subscribe(response => {
			if(response.fromUserId==this.selectedUserId || response.toUserId==this.selectedUserId){
				this.messages = [...this.messages, response];
			}
		});
	}

	userjoin() {
		this.messageService.userjoin(this.userId);
	}

	onUserSelected(user) {
		this.selectedUser = user;
		this.selectedUserId = user.id;
		this.selectedUserName = user.name;
		this.getMessageListByUserId();
	}

	onSendMessage(message: string) {
		if(message!=""){
			const data: Message = {
				fromUserId: this.userId,
			    toUserId: this.selectedUserId,
			    message:message.trim()
			};
			this.messageService.sendMessage(data);
			this.messages = [...this.messages, data];
  		}
  	}

  	getMessageListByUserId() {
		const params = {user_id: this.selectedUserId};
		this.messageService.getMessageListByUserId(params).subscribe(response => {
			if (response.success) {
				this.messages = response.data;
			} else {
				console.log("Error Message",response.message);
			}
		}, error => {
			console.log("Error Message",error.message);
		});
	}

	ngOnDestroy(){
		this.messageService.disconnect(this.userId);
	}
}
