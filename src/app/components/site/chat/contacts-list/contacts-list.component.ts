import { Component, OnInit, EventEmitter, Output,Input } from '@angular/core';
import { MessageService } from 'src/app/service/message.service';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-contacts-list',
  templateUrl: './contacts-list.component.html',
  styleUrls: ['./contacts-list.component.css']
})
export class ContactsListComponent implements OnInit {

    userList = [];
    activeUserId:number = 0;
    @Output() selectedUser = new EventEmitter<any>();
    @Input() defaultSelectedUser = 0;
    constructor(private messageService: MessageService, private userService: UserService) { }

    ngOnInit(): void {
        this.messageService.userdisconnect().subscribe(response => {
            this.userList.forEach((element,index)=>{
                if(element.id==response){
                    this.userList[index].status = 0;
                }
            });
        }); 
        this.messageService.newUserJoin().subscribe(response => {
            this.userList.forEach((element,index)=>{
                if(element.id==response){
                    this.userList[index].status = 1;
                }
            });
        });
        this.getUserList();
    }

    getUserList() {
        this.messageService.getUserList().subscribe(response => {
            if (response.success) {
                this.userList = response.data;
                if(this.defaultSelectedUser!==0){
                    const user = response.data.find(element=>element.id===this.defaultSelectedUser);
                    this.onUserSelected(user);                    
                }
            }
        }, error => {
            //this.authService.showErrorMessage(error.message);
        });
    }
    onUserSelected(user) {
        
        if(this.activeUserId !== user.id){
            this.activeUserId = user.id;
            const userdata = {"id":this.activeUserId,"name":user.name,"image":user.image};
            this.selectedUser.emit(userdata);
        }
    }
}
