import { Component, OnInit, Input, EventEmitter, Output, ViewChild, ElementRef,SimpleChanges } from '@angular/core';
import { UserService } from 'src/app/service/user.service';
import { MessageService } from 'src/app/service/message.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-messages-view',
  templateUrl: './messages-view.component.html',
  styleUrls: ['./messages-view.component.css']
})

export class MessagesViewComponent implements OnInit {
	currentUserId = 0;
	@Input() messages: [];
	@Input() selectedUser = {id:"",name:"",image:""};
	@Output() sendMessage = new EventEmitter<string>();
	@ViewChild('scrollMe', { static: false })
	messagesContainer: ElementRef<HTMLDivElement>;
   
	logintype= "employees";
	constructor(private messageService: MessageService, private userService: UserService,private router:Router) { }
	ngOnInit(): void {
		if(this.router.url.search("company") > -1){
			this.logintype = "employer";
		}
		this.currentUserId  = Number(this.userService.getUserid());
    }
	onSendMessage(message: string) {
		if(this.selectedUser.id != "" && this.selectedUser.id != undefined){
	  		this.sendMessage.emit(message);
		}
	}
	ngOnChanges(changes: SimpleChanges): void {
	  	if (changes.messages) {
			setTimeout(()=>{this.scrollIntoView();},10);
	  	}
	}
	private scrollIntoView() {
	  	if (this.messagesContainer) {
	    	const { nativeElement } = this.messagesContainer;
	    	nativeElement.scrollTop = nativeElement.scrollHeight;
	  	}
	}
}
