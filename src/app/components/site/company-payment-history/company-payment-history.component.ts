import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { TimesheetService } from 'src/app/service/timesheet.service';

@Component({
  selector: 'app-company-payment-history',
  templateUrl: './company-payment-history.component.html',
  styleUrls: ['./company-payment-history.component.css']
})
export class CompanyPaymentHistoryComponent implements OnInit {
 
  page = 1;
  paymentHistory = [];
  totalRecord = 0;
  constructor(
    private timeSheet:TimesheetService,
    private toastrService:ToastrService
  ) { }

  ngOnInit(): void {
      this.getPaymentHistory();
  }


  getPaymentHistory(): void {
    this.timeSheet.getPaymentHistory(this.page).subscribe((response) => {
      if(response.success) { 
        this.paymentHistory = response.data.payment_history;
        this.totalRecord = response.data.total_record;
      }
      else { 
        this.toastrService.success("Some");
      }
    },(error) => { 
      console.log("Get Payment History API Error",error);
    });
  }

  pageChanged($event):void{
    console.log($event);
    this.page = $event.page;
    this.getPaymentHistory();
  }
}
