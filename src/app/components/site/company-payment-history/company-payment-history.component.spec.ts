import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyPaymentHistoryComponent } from './company-payment-history.component';

describe('CompanyPaymentHistoryComponent', () => {
  let component: CompanyPaymentHistoryComponent;
  let fixture: ComponentFixture<CompanyPaymentHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyPaymentHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyPaymentHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
