import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TimesheetService } from 'src/app/service/timesheet.service';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-company-timesheets',
  templateUrl: './company-timesheets.component.html',
  styleUrls: ['./company-timesheets.component.css'],
})

export class CompanyTimesheetsComponent implements OnInit {
  timesheetList: any[] = [];
  selectedDate: Date;
  currentDate: Date;
  fromTime: Date;
  toTime: Date;
  totalHour = '0';
  totalMinutes = 0;
  totalAmount = 0;
  outerIndex = 0;
  innerIndex = 0;
  totalHourAmount = 0;
  selectedTimesheetId = 0;
  startDate = '';
  endDate = '';
  search = '';
  isSelectAll = false;
  ismeridian = false;
  submitted = false;
  datesList: Array<string> = [];
  dateData: any[] = [];
  selectedUserList: any[] = [];
  calculateTime: any[] = [];
  selectedUserTimesheet: any[] = [];
  updateTimesheetForm: FormGroup;
  @ViewChild('myModal') myModal;
  @ViewChild('timesheetModal') timesheetModal;
  selectedTimeHours: any = {
    id: 0,
    startTime: '',
    endTime: '',
  };
  user: any = {
    id: 0,
    image: '',
    hourrate: '',
    name: '',
    hour: '',
    price: 0,
  };

  constructor(
    private timesheetService: TimesheetService,
    private userService: UserService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.currentDate = new Date();
    this.setDateList(this.timesheetService.changeDateFormat(this.currentDate));
    this.updateTimesheetForm = this.formBuilder.group({
      startTime: ['', Validators.required],
      endTime: ['', Validators.required],
    });
  }

  setDateList(date: string): void {
    this.datesList = [];
    for (let i = 0; i < 7; i++) {
      this.datesList.push(this.timesheetService.changeDateFormat(new Date(new Date(date).setDate(new Date(date).getDate() + i))));
    }
    this.startDate = this.datesList[0];
    this.endDate = this.datesList[6];
    const params = { dates: this.datesList };
    this.timesheetService.getCompanyTimesheet(params).subscribe((response) => {
      if (response.success) {
        this.timesheetList = response.data;
      } else {
        this.userService.showErrorMessage(response.message);
      }
    });
  }

  onChangeSearchText($event): void {
    if ($event.key === 'Enter') {
      this.onSearch();
    }
  }

  onSearch(){
    this.setDateList(this.timesheetService.changeDateFormat(this.startDate));
  }

  onCheck(id: number, isChecked: boolean): void {
      const elementIndex = this.selectedUserList.findIndex(( ele ) => ( ele === id ));
      if (isChecked && elementIndex === -1) {
          this.selectedUserList.push(id);
      }
      else if (!isChecked && elementIndex > -1) {
        this.selectedUserList.splice(elementIndex, 1 );
      }
      this.isSelectAll = this.timesheetList.length === this.selectedUserList.length;
      this.calculateHour();
  }

  selectAll(isChecked: boolean): void {
    this.selectedUserList = isChecked ? this.timesheetList.map((value) => value.id) : [];
    this.isSelectAll = isChecked;
    this.calculateHour();
  }

  onChangeDate(type: number): void {
    const date = type === 1 ? new Date(new Date(this.startDate).getTime() + 604800000) : new Date(new Date(this.startDate).getTime() - 604800000);
    this.setDateList(this.timesheetService.changeDateFormat(date));
  }

  onApprove() {
    if (this.selectedUserList.length === 0) {
      this.userService.showErrorMessage('Please select at least one checkbox');
      return false;
    }
    this.timesheetService
      .approveTimesheetHours({selectedId: this.selectedUserList})
      .subscribe((response) => {
        if (response.success) {
          this.setDateList(this.timesheetService.changeDateFormat(this.startDate));
          this.userService.showSuccessMessage(response.message);
        } else {
          this.userService.showErrorMessage(response.message);
        }
      });
  }

  calculateHour(): void {
    this.totalMinutes = 0;
    this.totalAmount = 0;
    this.selectedUserList.forEach( ( ele ) => {
      const findElement = this.timesheetList.find((timesheet) => ( timesheet.id === ele ));
      if (findElement !== undefined) {
        this.totalAmount += findElement.totalamount;
        this.totalMinutes += findElement.minutes;
      }
    });
    this.totalHour = '';
    if (Math.floor(this.totalMinutes / 60)) {
        this.totalHour = Math.floor(this.totalMinutes / 60) + ' Hours ';
    }
    if (Math.floor(this.totalMinutes % 60)) {
      this.totalHour += Math.floor(this.totalMinutes % 60) + ' Minutes ';
    }
  }

  decimalNumber(price:number): string{
    return this.timesheetService.decimalNumber(price);
  }


    // setDateList(date):void {
  // 	this.datesList = [];
  // 	this.dateData = [];
  // 	let isPresent = false;
  // 	let startTime = '';
  // 	let endTime = '';
  // 	let id = 0;
  // 	let isCurrentMonth=0;
  // 	for (let i = 0; i < 7; i++) {
  // 		this.datesList.push(this.timesheetService.changeDateFormat(new Date(new Date(date).setDate(new Date(date).getDate() + i))));
  // 	}
  // 	this.startDate = this.datesList[0];
  // 	this.endDate = this.datesList[6];
  // 	const params = {startDate:this.startDate,endDate:this.endDate,searchText:this.search};
  // 	this.timesheetService.getCompanyTimesheet(params).subscribe(response=>{
  // 		if(response.success){
  // 			for(let index of Object.keys(response.data)){
  // 				let data = [];
  // 				this.selectedUserList.push({userId:0,check:false});
  // 				this.datesList.forEach((element) => {
  // 					isPresent = false;
  // 					isCurrentMonth = 0;
  // 					startTime = '';
  // 					endTime = '';
  // 					let findElement = response.data[index].timesheet.find(ele=>(ele.date==element));
  // 					if(findElement){
  // 						id = findElement.id;
  // 						isCurrentMonth=findElement.isCurrentMonth;
  // 						isPresent=true;
  // 						startTime=findElement.startTime;
  // 						endTime=findElement.endTime;
  // 					}
  // 					data.push({id,isPresent,isCurrentMonth,startTime,endTime,date:element});
  // 				});
  // 				this.dateData.push({
  // 					id:response.data[index].id,
  // 					name:response.data[index].name,
  // 					image:response.data[index].image,
  // 					address:response.data[index].address,
  // 					hourrate:response.data[index].hourrate,
  // 					timesheet:data
  // 				});
  // 			}
  // 		}
  // 		else{
  // 			this.userService.showErrorMessage(response.message);
  // 		}
  // 	});
  // }


  // openUpdateTimesheetModal(outerIndex, innerIndex, dates): void {
  //   let time = dates.startTime.split(':');
  //   let date = new Date();
  //   date.setHours(time[0]);
  //   date.setMinutes(time[1]);
  //   let startTime = new Date(date);
  //   time = dates.endTime.split(':');
  //   date.setHours(time[0]);
  //   date.setMinutes(time[1]);
  //   let endTime = new Date(date);
  //   this.updateTimesheetForm.patchValue({ startTime, endTime });
  //   this.selectedTimeHours.id = dates.id;
  //   this.outerIndex = outerIndex;
  //   this.innerIndex = innerIndex;
  //   this.modalRef = this.modalService.open(this.myModal);
  // }

  // onUpdateTimesheetFormSubmit(): void {
  //   if (this.updateTimesheetForm.invalid) {
  //     return;
  //   }
  //   this.selectedTimeHours.startTime = this.timesheetService.changeTimeFormat(
  //     this.updateTimesheetForm.value.startTime
  //   );
  //   this.selectedTimeHours.endTime = this.timesheetService.changeTimeFormat(
  //     this.updateTimesheetForm.value.endTime
  //   );
  //   this.timesheetService
  //     .updateTimeSheetHourById(this.selectedTimeHours)
  //     .subscribe((response) => {
  //       if (response.success) {
  //         this.dateData[this.outerIndex].timesheet[
  //           this.innerIndex
  //         ].startTime = this.selectedTimeHours.startTime;
  //         this.dateData[this.outerIndex].timesheet[
  //           this.innerIndex
  //         ].endTime = this.selectedTimeHours.endTime;
  //         this.modalService.close(this.modalRef);
  //         this.userService.showSuccessMessage(response.message);
  //       } else {
  //         this.userService.showErrorMessage(response.message);
  //       }
  //     });
  // }

  // calculateHour(): void {
  //   this.calculateTime = [];
  //   this.selectedUserList.forEach((element) => {
  //     if (element.check && element.userId !== 0) {
  //       const findElement = this.dateData.find((ele) => ele.id === element.userId);
  //       if (findElement) {
  //         let minutes = 0;
  //         findElement.timesheet.forEach(( ele ) => {
  //           if (ele.startTime !== '' && ele.endTime !== '') {
  //             minutes += this.calulateMinute(
  //               ele.startTime,
  //               ele.endTime
  //             );
  //           }
  //         });
  //         this.calculateTime.push({ minutes, hourrate: findElement.hourrate });
  //       }
  //     }
  //   });
  //   let totalMinutes = 0;
  //   this.totalHourAmount = 0;
  //   this.calculateTime.forEach((element) => {
  //     totalMinutes += element.minutes;
  //     this.totalHourAmount += Number(
  //       Math.round(element.minutes * (element.hourrate / 60))
  //     );
  //   });
  //   this.totalHour = this.hourConvertToMintues(totalMinutes);
  // }

  // calulateMinute(startTime, endTime): number {
  //   let time = startTime.split(':');
  //   let startdate = new Date();
  //   startdate.setHours(time[0]);
  //   startdate.setMinutes(time[1]);

  //   time = endTime.split(':');
  //   let enddate = new Date();
  //   enddate.setHours(time[0]);
  //   enddate.setMinutes(time[1]);
  //   let diff = (enddate.getTime() - startdate.getTime()) / 1000;
  //   diff /= 60;
  //   return Math.abs(Math.round(diff));
  // }

  // hourConvertToMintues(minutes): string {
  //   let text = (Math.floor(minutes / 60) + ' Hours ').toString();
  //   text += (Math.floor(minutes % 60) + ' Minutes').toString();
  //   return text;
  // }

  // openPopup(id): void {
  //   let text = '';
  //   let price = 0;
  //   let minutes = 0;
  //   this.selectedUserTimesheet = [];
  //   const data = { id, dates: this.datesList };
  //   this.user.price = 0;
  //   this.user.hour = '';
  //   this.user.id = 0;
  //   this.user.name = '';
  //   this.timesheetService.getTimesheetDetail(data).subscribe((response) => {
  //     if (response.success) {
  //       this.user.hourrate = response.data.user.hourrate;
  //       this.user.id = response.data.user.id;
  //       this.user.image = response.data.user.image;
  //       this.user.name = response.data.user.name;
  //       response.data.timesheet.forEach((element) => {
  //         text = this.hourConvertToMintues(
  //           this.calulateMinute(element.startTime, element.endTime)
  //         );
  //         minutes += this.calulateMinute(element.startTime, element.endTime);
  //         price = Math.round(
  //           (response.data.user.hourrate / 60) *
  //             this.calulateMinute(element.startTime, element.endTime)
  //         );
  //         this.selectedUserTimesheet.push({
  //           hour: text,
  //           startTime: element.startTime,
  //           endTime: element.endTime,
  //           date: element.date,
  //           price: price,
  //         });
  //         this.user.price += price;
  //       });
  //       this.user.hour = this.hourConvertToMintues(minutes);
  //       this.timesheetModalRef = this.modalService.open(this.timesheetModal, {
  //         size: 'lg',
  //       });
  //     } else {
  //       this.userService.showErrorMessage(response.message);
  //     }
  //   });
  // }




}
