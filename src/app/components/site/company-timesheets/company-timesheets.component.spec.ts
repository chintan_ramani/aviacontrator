import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyTimesheetsComponent } from './company-timesheets.component';

describe('CompanyTimesheetsComponent', () => {
  let component: CompanyTimesheetsComponent;
  let fixture: ComponentFixture<CompanyTimesheetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyTimesheetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyTimesheetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
