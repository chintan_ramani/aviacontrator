import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { JobService } from 'src/app/service/job.service';
import { UserService } from 'src/app/service/user.service';

@Component({
	selector: 'app-job-details',
	templateUrl: './job-details.component.html',
	styleUrls: ['./job-details.component.css']
})

export class JobDetailsComponent implements OnInit {

	jobId: string = "";
	imagePreview: string = "";
	errorMessage: string = "";
	jobDetails:any = {
		rate: "",
		image: "",
		position: "",
		location: "",
		dayShift: "",
		nightShift: "",
		description:"",
		hourPerDay: "",
		contractEnd: "",
		contractStart: "",
		siftRosterOn: 0,
		siftRosterOff: 0,
		siftRosterOnTime: "",
		siftRosterOffTime: "",
	}
	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private jobService: JobService,
		private userService: UserService,
	) { }

	ngOnInit(): void {
		this.route.paramMap.subscribe((paramMap: ParamMap) => {
			this.jobId = paramMap.get('jobId');
			this.getJobDetailsById();
		});
	}

	getJobDetailsById(): void {
		this.jobService.getJobDetailsById(this.jobId).subscribe(response => {
			if (response.success) {
				this.jobDetails.image = response.data.image;
				this.jobDetails.position = response.data.position;
				this.jobDetails.description = response.data.jobDescription;
				this.jobDetails.location = response.data.location;
				this.jobDetails.siftRosterOn = response.data.siftRosterOn;
				this.jobDetails.siftRosterOnTime = response.data.siftRosterOnTime;
				this.jobDetails.siftRosterOff = response.data.siftRosterOff;
				this.jobDetails.siftRosterOffTime = response.data.siftRosterOffTime;
				this.jobDetails.hourPerDay = response.data.hourPerDay;
				this.jobDetails.dayShift = response.data.dayShift;
				this.jobDetails.nightShift = response.data.nightShift;
				this.jobDetails.rate = response.data.rate;
				this.jobDetails.contractStart = response.data.contractStart;
				this.jobDetails.contractEnd = response.data.contractEnd;
			}
			else {
				this.jobService.showErrorMessage(response.message);
			}
		}, (error) => {
			this.jobService.showErrorMessage("Something went wrong,Please try again later...");
		});
	}
	redirectToJob() {
		if(this.userService.loggedIn() && this.userService.getUserType()==1) {
			this.router.navigateByUrl(`contractor/jobs?jobId=${this.jobId}`);
		}
		else {
			this.jobService.setJobId(this.jobId);
			this.router.navigateByUrl(`contractor-login`);
		}
	}
}
