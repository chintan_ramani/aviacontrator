import { FormValidatorService } from 'src/app/service/form-validator.service';
import { Component, OnInit} from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/service/user.service';

@Component({
	selector: 'app-forgot-password',
	templateUrl: './forgot-password.component.html',
	styleUrls: ['./forgot-password.component.css']
})

export class ForgotPasswordComponent implements OnInit {
	
	forgotPasswordForm: FormGroup;	
	constructor(
		private formBuilder: FormBuilder,
		private userService: UserService,
		private formValidatorService: FormValidatorService,
		private router:Router
	) { }
	
	ngOnInit():void {
		this.forgotPasswordForm = this.formBuilder.group({
			email: ['', [Validators.required, Validators.email]]
		});
	}
	
	onSubmit():void {
		if (this.forgotPasswordForm.valid) {
			this.userService.forgotPassword(this.forgotPasswordForm.value).subscribe(response => {
				if (response.success) {
					this.userService.showSuccessMessage(response.message);
					this.router.navigate(["/login"]);
				}
				else {
					this.userService.showErrorMessage(response.message);
				}
			});
		} else {
			this.formValidatorService.validateAllFormFields(this.forgotPasswordForm);
		}
	}
}
