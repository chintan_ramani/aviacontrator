import { FormValidatorService } from 'src/app/service/form-validator.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { UserService } from 'src/app/service/user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JobService } from 'src/app/service/job.service';

@Component({
	selector: 'app-contractor-login',
	templateUrl: './contractor-login.component.html',
	styleUrls: ['./contractor-login.component.css']
})

export class ContractorLoginComponent implements OnInit {
	loginForm: FormGroup;
	constructor(
		private router: Router,
		private formBuilder: FormBuilder,
		private userService: UserService,
		private jobService: JobService,
		private formValidatorService: FormValidatorService
	) { }

	ngOnInit():void {
		this.loginForm = this.formBuilder.group({
			email: ['', [Validators.required, Validators.email]],
			password: ['', Validators.required],
			userType: ['1']
		});
	}

	onSubmit():void {
		if (this.loginForm.valid) {
			this.userService.login(this.loginForm.value).subscribe(response => {
				if (response.success) {
					this.userService.showSuccessMessage(response.message);
					this.userService.saveAuthData(response.data);
					if(this.jobService.getJobId() !== 0){
						console.log(`/contractor/jobs?jobId=${this.jobService.getJobId()}`);
						this.router.navigateByUrl(`/contractor/jobs?jobId=${this.jobService.getJobId()}`);
						this.jobService.removeJobId();
					}
					else {
						this.jobService.removeJobId();
						this.router.navigate(["/contractor/jobs"]);
					}
				}
				else {
					this.userService.showErrorMessage(response.message);
				}
			});
		} else {
			this.formValidatorService.validateAllFormFields(this.loginForm);
		}
	}
}
