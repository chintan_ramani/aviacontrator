import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { UserService } from 'src/app/service/user.service';

@Component({
	selector: 'app-verify',
	templateUrl: './verify.component.html',
	styleUrls: ['./verify.component.css']
})
export class VerifyComponent implements OnInit {

	token: string;
	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private userService: UserService
	) { }

	ngOnInit(): void {
		this.route.paramMap.subscribe((paramMap: ParamMap) => {
			this.token = paramMap.get('token');
			this.verifyToken();
		});
	}

	verifyToken(): void {
		this.userService.verifyToken({token:this.token}).subscribe((response) => {
			if(response.success){
				this.userService.showSuccessMessage(response.message);
			}
			else{
				this.userService.showErrorMessage(response.message);
			}
			this.router.navigate(['login']);
		}, (error) => {
			console.log(`Verify Token Api Error ${error}`);
		});
	}
}
