import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import {
	FormGroup,
	FormArray,
	Validators,
	FormBuilder,
	FormControl,
} from '@angular/forms';
import { DatePipe } from '@angular/common';
import { JobService } from 'src/app/service/job.service';
import { UserService } from 'src/app/service/user.service';
import { FormValidatorService } from 'src/app/service/form-validator.service';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { ModalManager } from 'ngb-modal';
import { Router } from '@angular/router';

@Component({
	selector: 'app-company-post-job',
	templateUrl: './company-post-job.component.html',
	styleUrls: ['./company-post-job.component.css'],
})

export class CompanyPostJobComponent implements OnInit {
	private modalRef;
	private paymentModalRef;
	@ViewChild('placesRef') placesRef: GooglePlaceDirective;
	@ViewChild('paymentModal') paymentModal: ElementRef;
	@ViewChild('myModal') myModal: ElementRef;
	stripeForm: FormGroup;
	postjobform: FormGroup;
	latitude = '0';
	longitude = '0';
	paymentError = '';
	paymentErrorMessage = '';
	isActivePlan = 0;
	selectedPlanId = 0;
	isAddressChange = 0;
	min: Date = new Date();
	minEndDate: Date = new Date();
	planList: any[] = [];
	stripeFormSubmited = false;
	freePlanSelected = false;
	previousPlanExpiry = false;
	cardPayment = false;
	showAlertBox = false;
	validLocation = false;
	pattern: string = "^[0-9]+(.[0-9]{1,2})?$";

	constructor(
		private router: Router,
		private jobservice: JobService,
		private userservice: UserService,
		private formBuilder: FormBuilder,
		private modalService: ModalManager,
		private formValidatorService: FormValidatorService,
	) { }

	ngOnInit(): void {
		this.min.setDate(this.min.getDate() - 1);
		this.minEndDate.setDate(this.minEndDate.getDate() - 1);
		this.stripeForm = this.formBuilder.group({
			cardNumber: ['', [Validators.required, Validators.pattern('[0-9]+')]],
			cardMonth: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(2)]],
			cardYear: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(4), Validators.pattern('[0-9]+')]],
			cardCVV: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(4), Validators.pattern('[0-9]+')]],
		});

		this.jobservice.getPlanList().subscribe((response) => {
			if (response.success) {
				this.freePlanSelected = response.data.freePlanSelected;
				this.previousPlanExpiry = response.data.previousPlanExpiry;
				this.planList = response.data.planList;
				this.planList.forEach((element) => {
					if (element.isActivePlan === 1) {
						this.isActivePlan = 1;
					}
				});
				if (!response.data.isActivePlan) {
					this.openModel();
				}
				this.showAlertBox = response.data.isProfileCompleted ? false : true;
			} else {
				this.jobservice.showErrorMessage(response.message);
			}
		}, (error) => {
			console.log(`Get plan list api ${error}`);
		});

		this.postjobform = this.formBuilder.group({
			type: ['hourly'],
			position: ['', [Validators.required]],
			location: ['', [Validators.required]],
			jobDescription: ['', [Validators.required]],
			siftRosterOn: [false],
			siftRosterOnTime: [{ value: '', disabled: true },[Validators.pattern(this.pattern)]],
			siftRosterOff: [false],
			siftRosterOffTime: [{ value: '', disabled: true },[Validators.pattern(this.pattern)]],
			hourPerDay: ['',[Validators.required, Validators.pattern(this.pattern)]],
			dayShift: [false],
			nightShift: [false],
			bothShift: [false],
			rate: ['',[Validators.required, Validators.pattern(this.pattern)]],
			contractStart: ['', [Validators.required]],
			contractEnd: ['', [Validators.required]],
			cv: [false],
			license: [false],
			reference: [false],
			hf: [false],
			cdccl: [false],
			ewis: [false],
			other: [false],
			otherText: this.formBuilder.array([]),
		});
		this.addOtherText();
		this.postjobform.get('siftRosterOn').valueChanges.subscribe((change) => {
			if (change) {
				this.postjobform
					.get('siftRosterOnTime')
					.setValidators([
						Validators.pattern(this.pattern),
						Validators.required,
					]);
				this.postjobform.get('siftRosterOnTime').enable();
			} else {
				this.postjobform.get('siftRosterOnTime').setValidators(null);
				this.postjobform.get('siftRosterOnTime').disable();
				this.postjobform.patchValue({ siftRosterOnTime: '' });
			}
			this.postjobform.get('siftRosterOnTime').updateValueAndValidity();
		});
		this.postjobform.get('siftRosterOff').valueChanges.subscribe((change) => {
			if (change) {
				this.postjobform
					.get('siftRosterOffTime')
					.setValidators([
						Validators.pattern(this.pattern),
						Validators.required,
					]);
				this.postjobform.get('siftRosterOffTime').enable();
			} else {
				this.postjobform.get('siftRosterOffTime').setValidators(null);
				this.postjobform.get('siftRosterOffTime').disable();
				this.postjobform.patchValue({ siftRosterOffTime: '' });
			}
			this.postjobform.get('siftRosterOffTime').updateValueAndValidity();
		});
		this.postjobform.get('bothShift').valueChanges.subscribe((change) => {
			let isChange = change ? true : false;
			this.postjobform.patchValue({
				dayShift: isChange,
				nightShift: isChange,
			});
		});
	}

	get otherTextList(): FormArray {
		return this.postjobform.controls.otherText as FormArray;
	}

	selectPlan(): void {
		this.cardPayment = true;
	}

	addOtherText(): void {
		this.otherTextList.push(new FormControl(''));
	}

	removeOtherText(index: number): void {
		this.otherTextList.removeAt(index);
	}

	onStartChange($event): void {
		this.minEndDate = $event;
		if (
			this.postjobform.value.contractEnd != '' &&
			new Date(this.postjobform.value.contractEnd).getTime() <
			new Date($event).getTime()
		) {
			this.postjobform.patchValue({ contractEnd: '' });
		}
	}

	onSubmit(): void {
		if (this.postjobform.valid) {
			if (
				(this.latitude == '' && this.longitude == '') ||
				(this.latitude == '0' && this.longitude == '0') ||
				this.isAddressChange == 1
			) {
				this.validLocation = true;
				return;
			}
			this.validLocation = false;
			let params = { latitude: this.latitude, longitude: this.longitude };
			for (let key in this.postjobform.value) {
				params = { ...params, ...{ [key]: ((key == 'contractStart' || key == 'contractEnd') ? new DatePipe('en-US').transform(this.postjobform.value[key], 'y-M-d') : this.postjobform.value[key]) } };
			}
			this.jobservice.postJob(params).subscribe((response) => {
				if (response.success) {
					this.postjobform.reset();
					this.otherTextList.clear();
					this.addOtherText();
					this.jobservice.showSuccessMessage(response.message);
					this.router.navigate(['/company/my-jobs']);
				} else {
					this.jobservice.showErrorMessage(response.message);
				}
			});
		} else {
			this.formValidatorService.validateAllFormFields(this.postjobform);
		}
	}

	changeAddress(): void {
		this.isAddressChange = 1;
	}

	onChangeAddress(address: any): void {
		this.latitude = address.geometry.location.lat();
		this.longitude = address.geometry.location.lng();
		this.postjobform.patchValue({ location: address.formatted_address });
		this.isAddressChange = 0;
		this.validLocation = false;
	}

	openModel(): void {
		this.modalRef = this.modalService.open(this.myModal, {
			size: 'lg',
			modalClass: 'payment-modal modal-lg',
		});
	}

	openPaymentModel(planId: number): void {
		this.stripeFormSubmited = false;
		this.stripeForm.reset();
		this.selectedPlanId = planId;
		this.paymentError = '';
		this.paymentModalRef = this.modalService.open(this.paymentModal, { size: 'md', backdrop: 'static' });
	}

	onSelectFreePlan(jobPlanId: string, freePlanSelected: boolean): void {
		if (!freePlanSelected) {
			this.modalService.close(this.modalRef);
			this.userservice.userSelectedPlan({ jobPlanId }).subscribe((response) => {
				if (response.success) {
					this.userservice.showSuccessMessage(response.message);
				} else {
					this.userservice.showErrorMessage(response.message);
				}
			}, (error) => {
				console.log(`User selected plan api error ${error}`);
			});
		}
	}

	closePaymentModel(): void {
		this.modalService.close(this.paymentModalRef);
	}

	stripePayment(stripeToken: string): void {
		this.jobservice.stripePayment({ stripeToken, selectedPlanId: this.selectedPlanId }).subscribe((response) => {
			const messageElement = document.querySelector("#message");
			if (response) {
				this.modalService.close(this.paymentModalRef);
				this.modalService.close(this.modalRef);
				this.userservice.showSuccessMessage(response.message);
				messageElement.innerHTML = '';
			}
			else {
				messageElement.innerHTML = response.message;
				this.paymentError = response.message;
			}
		}, (error) => {
			console.log(`Stripe Payment Api Error ${error}`);
		});
	}

	loadStripe(): void {
		if (!window.document.getElementById('stripe-script')) {
			var s = window.document.createElement("script");
			s.id = "stripe-script";
			s.type = "text/javascript";
			s.src = "https://checkout.stripe.com/checkout.js";
			window.document.body.appendChild(s);
		}
	}


	getToken(): void {
		this.stripeFormSubmited = true;
		if (this.stripeForm.invalid) {
			return;
		}
		const messageElement = document.querySelector("#message");
		messageElement.innerHTML = '';
		this.paymentErrorMessage = '';
		(<any>window).Stripe.card.createToken({
			number: this.stripeForm.value.cardNumber,
			exp_month: this.stripeForm.value.cardMonth,
			exp_year: this.stripeForm.value.cardYear,
			cvc: this.stripeForm.value.cardCVV
		}, (status: number, response: any) => {
			if (status == 200) {
				messageElement.innerHTML = '';
				this.paymentErrorMessage = '';
				console.log(response, "Repsonse");
				this.stripePayment(response.id);
			}
			else {
				messageElement.innerHTML = response.error.message;
				console.log(response.error.message);
				this.paymentErrorMessage = response.error.message;
				console.log(this.paymentErrorMessage);
			}
		});
	}
}

