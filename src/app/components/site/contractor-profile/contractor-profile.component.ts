import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { FormValidatorService } from 'src/app/service/form-validator.service';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-contractor-profile',
  templateUrl: './contractor-profile.component.html',
  styleUrls: ['./contractor-profile.component.css'],
})

export class ContractorProfileComponent implements OnInit {
  userImage;
  profileForm: FormGroup;
  showImage = false;
  isAddressChange = 0;
  latitude = '0';
  longitude = '0';
  countCvDocument = 0;
  countPassportDocument = 0;
  cvDocumentErrorMessage = '';
  passportDocumentErrorMessage = '';
  hfArray: Array<string> = [];
  CvArray: Array<string> = [];
  ewisArray: Array<string> = [];
  cdcclArray: Array<string> = [];
  licenseArray: Array<string> = [];
  passportArray: Array<string> = [];
  experienceArray: Array<string> = [];
  certificatesArray: Array<string> = [];
  criminalRecordArray: Array<string> = [];
  referenceFormsArray: Array<string> = [];
  medicalCertificateArray: Array<string> = [];
  aircraftCertificatesArray: Array<string> = [];
  fileTypes: Array<string> = ['jpg', 'jpeg', 'png'];
  documentTypes: Array<string> = ['pdf', 'docx', 'doc'];
  @ViewChild('file') file: HTMLElement;
  @ViewChild('image') image: HTMLImageElement;
  @ViewChild('placesRef', { static: false }) placesRef: GooglePlaceDirective;

  constructor(
    private formBuilder: FormBuilder,
    private formValidatorService: FormValidatorService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.profileForm = this.formBuilder.group({
      companyName: ['', [Validators.required]],
      address: ['', [Validators.required]],
      companyCode: ['', [Validators.required]],
      countryResidence: ['', [Validators.required]],
    //  taxes: ['', [Validators.required]],
      accountNumber: ['', [Validators.required]],
      shortCode: ['', [Validators.required]],
      bankAddress: ['', [Validators.required]],
      isSelfEmployee: [false],
      hasCompany: [false],
      passportList: this.formBuilder.array([]),
      cvList: this.formBuilder.array([]),
      medicalCertificate: this.formBuilder.array([]),
      licenseList: this.formBuilder.array([]),
      certificatesList: this.formBuilder.array([]),
      criminalRecordList: this.formBuilder.array([]),
      hfList: this.formBuilder.array([]),
      ewisList: this.formBuilder.array([]),
      cdcclList: this.formBuilder.array([]),
      aircraftCertificatesList: this.formBuilder.array([]),
      referenceFormsList: this.formBuilder.array([]),
      experienceList: this.formBuilder.array([]),
      jobTitle: ['', [Validators.required]],
      hourrate: ['', [Validators.required]],
    });
    this.getUserDetail();
    this.addDocument(this.passportList);
    this.addDocument(this.cvList);
    this.addDocument(this.medicalCertificate);
    this.addDocument(this.licenseList);
    this.addDocument(this.certificatesList);
    this.addDocument(this.criminalRecordList);
    this.addDocument(this.hfList);
    this.addDocument(this.ewisList);
    this.addDocument(this.cdcclList);
    this.addDocument(this.aircraftCertificatesList);
    this.addDocument(this.referenceFormsList);
    this.addDocument(this.experienceList);
  }
  get passportList(): FormArray {
    return this.profileForm.controls.passportList as FormArray;
  }
  get cvList(): FormArray {
    return this.profileForm.controls.cvList as FormArray;
  }
  get medicalCertificate(): FormArray {
    return this.profileForm.controls.medicalCertificate as FormArray;
  }
  get licenseList(): FormArray {
    return this.profileForm.controls.licenseList as FormArray;
  }
  get certificatesList(): FormArray {
    return this.profileForm.controls.certificatesList as FormArray;
  }
  get criminalRecordList(): FormArray {
    return this.profileForm.controls.criminalRecordList as FormArray;
  }
  get hfList(): FormArray {
    return this.profileForm.controls.hfList as FormArray;
  }
  get cdcclList(): FormArray {
    return this.profileForm.controls.cdcclList as FormArray;
  }
  get ewisList(): FormArray {
    return this.profileForm.controls.ewisList as FormArray;
  }
  get aircraftCertificatesList(): FormArray {
    return this.profileForm.controls.aircraftCertificatesList as FormArray;
  }
  get referenceFormsList(): FormArray {
    return this.profileForm.controls.referenceFormsList as FormArray;
  }
  get experienceList(): FormArray {
    return this.profileForm.controls.experienceList as FormArray;
  }

  addDocumentFormGroup(): FormGroup {
    return this.formBuilder.group({
      documentName: [''],
      documentFile: [''],
    });
  }

  addDocument(list: any): void {
    list.push(this.addDocumentFormGroup());
  }

  removeDocument(index: number, list: any): void {
    list.removeAt(index);
  }

  onFileChange(files: any, index: number, list: any, elementId: string): void {
    if (files.length === 0) {
      return;
    }
    const extension = files[0].name.split('.').pop().toLowerCase();
    const isSuccess = this.documentTypes.indexOf(extension) > -1;
    if (isSuccess) {
      const fName = files[0].name;
      document.getElementsByClassName('' + elementId)[0].textContent = fName;
      list.controls.forEach((control, elementIndex) => {
        if (index === elementIndex) {
          control.value.documentFile = files[0];
        }
      });
    } else {
      this.userService.showErrorMessage('Please select only pdf,docx,doc file');
    }
  }

  getUserDetail(): void {
    this.userService.getUserDetail().subscribe((response) => {
      if (response.success) {
        this.profileForm.patchValue({
          companyName: response.data.companyName,
          companyCode: response.data.companyCode,
          countryResidence: response.data.countryResidence,
      //    taxes: response.data.taxes,
          accountNumber: response.data.accountNumber,
          shortCode: response.data.shortCode,
          bankAddress: response.data.bankAddress,
          isSelfEmployee: response.data.isSelfEmployee,
          hasCompany: response.data.hasCompany,
          address: response.data.address,
          jobTitle: response.data.jobTitle,
          hourrate: response.data.hourrate,
        });
        this.latitude = response.data.latitude;
        this.longitude = response.data.longitude;
        this.passportArray = JSON.parse(response.data.passport);
        this.CvArray = JSON.parse(response.data.cv);
        this.medicalCertificateArray = JSON.parse(
          response.data.medicalCertificate
        );
        this.licenseArray = JSON.parse(response.data.license);
        this.certificatesArray = JSON.parse(response.data.certificates);
        this.criminalRecordArray = JSON.parse(response.data.criminalRecord);
        this.hfArray = JSON.parse(response.data.hf);
        this.ewisArray = JSON.parse(response.data.ewis);
        this.cdcclArray = JSON.parse(response.data.cdccl);
        this.aircraftCertificatesArray = JSON.parse(
          response.data.aircraftCertificates
        );
        this.referenceFormsArray = JSON.parse(response.data.referenceForms);
        this.experienceArray = JSON.parse(response.data.experience);
        if (response.data.image) {
          this.userImage = response.data.image;
          this.showImage = true;
        }
      } else {
        this.userService.showErrorMessage(response.message);
      }
    });
  }

  onSelectImage(files: File[]): void {
    if (files.length === 0) {
      return;
    }
    const extension = files[0].name.split('.').pop().toLowerCase();
    const isSuccess = this.fileTypes.indexOf(extension) > -1;
    if (isSuccess) {
      const reader = new FileReader();
      reader.readAsDataURL(files[0]);
      reader.onload = (event) => {
        this.showImage = true;
        this.userImage = reader.result;
        this.fileUpload(files[0]);
      };
    } else {
      this.userService.showErrorMessage('Please select only jpg,jpeg,png file');
    }
  }

  fileUpload(file: File): void {
    const form = new FormData();
    form.append('image', file);
    this.userService.fileUpload(form).subscribe((response) => {
      if (!response.success) {
        this.userService.showErrorMessage(response.message);
      }
    });
  }

  onDeleteDocument(list: any, index: number, docs: string, file: string): void {
    const params = { docsName: docs, fileName: file };
    this.userService.deleteDocument(params).subscribe((response) => {
      if (response.success) {
        list.splice(index, 1);
        this.userService.showSuccessMessage(response.message);
      } else {
        this.userService.showErrorMessage(response.message);
      }
    });
  }

  onSubmit(): void {
    if (this.profileForm.valid) {
      if (
        (this.latitude === '' && this.longitude === '') ||
        (this.latitude === '0' && this.longitude === '0') ||
        this.isAddressChange === 1
      ) {
        this.userService.showErrorMessage('Please select address');
        return;
      }
      this.passportList.controls.forEach((control, index) => {
          if(control.value.documentFile != "") {
            this.countPassportDocument++
          }
      });   
      this.cvList.controls.forEach((control, index) => {
        if(control.value.documentFile != "") {
          this.countCvDocument++
        }
      });
      this.countCvDocument += this.CvArray.length;
      this.countPassportDocument += this.passportArray.length;
      if(this.countPassportDocument==0) {
        this.passportDocumentErrorMessage = "Please select passport document";
      }
      if(this.countCvDocument==0) {
        this.cvDocumentErrorMessage = "Please select cv document";
      } 
      if(this.countCvDocument==0 ||  this.countPassportDocument==0){
        window.scrollTo({
          top: 650,
          behavior: 'smooth',
        })
        return;
      }
      this.countCvDocument = 0;
      this.countPassportDocument = 0;
      this.passportDocumentErrorMessage = '';
      this.cvDocumentErrorMessage = '';
      const formData = new FormData();
      this.passportList.controls.forEach((control, index) => {
        formData.append(
          'passportList[' + index + ']',
          control.value.documentFile
        );
      });
      this.cvList.controls.forEach((control, index) => {
        formData.append('CvList[' + index + ']', control.value.documentFile);
      });
      this.medicalCertificate.controls.forEach((control, index) => {
        formData.append(
          'medicalCertificate[' + index + ']',
          control.value.documentFile
        );
      });
      this.licenseList.controls.forEach((control, index) => {
        formData.append(
          'licenseList[' + index + ']',
          control.value.documentFile
        );
      });
      this.certificatesList.controls.forEach((control, index) => {
        formData.append(
          'certificatesList[' + index + ']',
          control.value.documentFile
        );
      });
      this.criminalRecordList.controls.forEach((control, index) => {
        formData.append(
          'criminalRecordList[' + index + ']',
          control.value.documentFile
        );
      });
      this.hfList.controls.forEach((control, index) => {
        formData.append('hfList[' + index + ']', control.value.documentFile);
      });
      this.ewisList.controls.forEach((control, index) => {
        formData.append('ewisList[' + index + ']', control.value.documentFile);
      });
      this.cdcclList.controls.forEach((control, index) => {
        formData.append('cdcclList[' + index + ']', control.value.documentFile);
      });
      this.aircraftCertificatesList.controls.forEach((control, index) => {
        formData.append(
          'aircraftCertificatesList[' + index + ']',
          control.value.documentFile
        );
      });
      this.referenceFormsList.controls.forEach((control, index) => {
        formData.append(
          'referenceFormsList[' + index + ']',
          control.value.documentFile
        );
      });
      this.experienceList.controls.forEach((control, index) => {
        formData.append(
          'experienceList[' + index + ']',
          control.value.documentFile
        );
      });
      formData.append('companyName', this.profileForm.value.companyName);
      formData.append('companyCode', this.profileForm.value.companyCode);
      formData.append(
        'countryResidence',
        this.profileForm.value.countryResidence
      );
     // formData.append('taxes', this.profileForm.value.taxes);
      formData.append('accountNumber', this.profileForm.value.accountNumber);
      formData.append('shortCode', this.profileForm.value.shortCode);
      formData.append('bankAddress', this.profileForm.value.bankAddress);
      formData.append('isSelfEmployee', this.profileForm.value.isSelfEmployee);
      formData.append('hasCompany', this.profileForm.value.hasCompany);
      formData.append('latitude', this.latitude);
      formData.append('longitude', this.longitude);
      formData.append('address', this.profileForm.value.address);
      formData.append('hourrate', this.profileForm.value.hourrate);
      formData.append('jobTitle', this.profileForm.value.jobTitle);
      formData.append('userType', '1');
      this.userService.updateUserDetail(formData).subscribe((response) => {
        if (response.success) {
          this.ngOnInit();
          this.userService.showSuccessMessage(response.message);
        } else {
          this.userService.showErrorMessage(response.message);
        }
      });
    } else {
      this.formValidatorService.validateAllFormFields(this.profileForm);
      window.scrollTo({
        top: 0,
        behavior: 'smooth',
      });
    }
  }

  onChangeAddress(address: any): void {
    this.latitude = address.geometry.location.lat();
    this.longitude = address.geometry.location.lng();
    this.profileForm.patchValue({ address: address.formatted_address });
    this.isAddressChange = 0;
  }
}
