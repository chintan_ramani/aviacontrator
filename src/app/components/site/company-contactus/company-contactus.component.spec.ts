import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyContactusComponent } from './company-contactus.component';

describe('CompanyContactusComponent', () => {
  let component: CompanyContactusComponent;
  let fixture: ComponentFixture<CompanyContactusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyContactusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyContactusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
