import { Component, OnInit } from '@angular/core';
import { FormGroup,Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/service/user.service';
import { FormValidatorService } from 'src/app/service/form-validator.service';

@Component({
	selector: 'app-contractor',
	templateUrl: './contractor.component.html',
	styleUrls: ['./contractor.component.css']
})

export class ContractorComponent implements OnInit {

	registerForm: FormGroup;
	date: string = "";
	constructor(
		private router: Router,
		private formBuilder: FormBuilder,
		private userService:UserService,
		private formValidatorService:FormValidatorService
	) { }

	ngOnInit():void {
		this.registerForm = this.formBuilder.group({
			firstName: ['', [Validators.required]],
			lastName: ['', [Validators.required]],
			email: ['', [Validators.required, Validators.email]],
			mobileNumber: ['', [Validators.required, Validators.pattern('[0-9]+')]],
			day: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.min(1), Validators.max(31)]],
			month: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.min(1), Validators.max(12)]],
			year: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.minLength(4), Validators.maxLength(4)]],
			password: ['', Validators.required],
			confirmPassword: ['', Validators.required],
			userType: ['1'],
			agreement: [false, Validators.requiredTrue],
			dob: [""]
		},{validator: this.checkPasswords });
	}

	checkPasswords(group: FormGroup) { 
		let password = group.get('password').value;
		let confirmPassword = group.get('confirmPassword').value;
		return  password === confirmPassword ? null : { mustMatch: true }; 
    }
	onSubmit() :void{
		if(this.registerForm.valid){
			this.date = this.registerForm.controls.year.value + "/" + this.registerForm.controls.month.value + "/" + this.registerForm.controls.day.value;
			this.registerForm.controls.dob.setValue(this.date);
			this.userService.register(this.registerForm.value).subscribe(response => {
				if (response.success) {
					this.userService.showSuccessMessage(response.message);
					this.router.navigate(["/contractor-login"]);
				}
				else {
						this.userService.showErrorMessage(response.message);
				}                                
			});                        
		}
		else {                     
				this.formValidatorService.validateAllFormFields(this.registerForm);
		}
	}

	scroll(_element: HTMLElement):void {
		_element.scrollIntoView({ behavior: "smooth" });
	}	
}
