import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorFaqComponent } from './contractor-faq.component';

describe('ContractorFaqComponent', () => {
  let component: ContractorFaqComponent;
  let fixture: ComponentFixture<ContractorFaqComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractorFaqComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractorFaqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
