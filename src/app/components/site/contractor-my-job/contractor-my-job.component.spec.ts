import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorMyJobComponent } from './contractor-my-job.component';

describe('ContractorMyJobComponent', () => {
  let component: ContractorMyJobComponent;
  let fixture: ComponentFixture<ContractorMyJobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractorMyJobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractorMyJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
