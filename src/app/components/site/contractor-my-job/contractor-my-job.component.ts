import { Component, OnInit } from '@angular/core';
import { JobService } from 'src/app/service/job.service';
import { TimesheetService } from 'src/app/service/timesheet.service';

@Component({
	selector: 'app-contractor-my-job',
	templateUrl: './contractor-my-job.component.html',
	styleUrls: ['./contractor-my-job.component.css']
})

export class ContractorMyJobComponent implements OnInit {
	
	jobArray: any[] = [];
	constructor(
		private timesheetService:TimesheetService,
		private jobService: JobService
	) { }
	
	ngOnInit() {
		this.getMyJobs();
	}

	getMyJobs():void {
		this.timesheetService.setApprovedNotificationCouter(0);
		this.jobService.contractorGetMyjob().subscribe(response => {
			if (response.message) {
				this.jobArray = response.data;
			} else {
				this.jobService.showErrorMessage(response.message);
			}
		});
	}
}
