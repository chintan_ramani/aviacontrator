export interface Message {
    fromUserId: number;
    toUserId: number;
    message: string;
    createdAt?: string;
}
