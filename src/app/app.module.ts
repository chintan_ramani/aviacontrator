import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { OwlDateTimeModule, OwlNativeDateTimeModule, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { NgxSpinnerModule } from "ngx-spinner";
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { ModalModule } from 'ngb-modal';
import { ToastrModule } from 'ngx-toastr';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { DatePipe } from '@angular/common';
import { AgmCoreModule,GoogleMapsAPIWrapper } from '@agm/core';
import { environment } from 'src/environments/environment';
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';
import { NgxStripeModule } from 'ngx-stripe';
import { PaginationModule } from 'ngx-bootstrap/pagination';


/* Service */
import { FormValidatorService } from 'src/app/service/form-validator.service';
import { TokenInterceptorService } from './service/TokenInterceptorService';
import { UserService } from './service/user.service';

/* Component */
import { SiteHeaderComponent } from './components/layout/site-header/site-header.component';
import { SiteFooterComponent } from './components/layout/site-footer/site-footer.component';
import { SiteLayoutComponent } from './components/layout/site-layout/site-layout.component';
import { ContentLayoutComponent } from './components/layout/content-layout/content-layout.component';
import { HomeComponent } from './components/site/home/home.component';
import { LoginComponent } from './components/site/login/login.component';
import { PrivacyPolicyComponent } from './components/site/privacy-policy/privacy-policy.component';
import { TermsConditionComponent } from './components/site/terms-condition/terms-condition.component';
import { ForgotPasswordComponent } from './components/site/forgot-password/forgot-password.component';
import { ContactUsComponent } from './components/site/contact-us/contact-us.component';
import { ContractorLoginComponent } from './components/site/contractor-login/contractor-login.component';
import { CompanyLoginComponent } from './components/site/company-login/company-login.component';
import { CompanyComponent } from './components/site/company/company.component';
import { ContractorComponent } from './components/site/contractor/contractor.component';
import { RegisterComponent } from './components/site/register/register.component';
import { CompanyFaqComponent } from './components/site/company-faq/company-faq.component';
import { ContractorFaqComponent } from './components/site/contractor-faq/contractor-faq.component';
import { ContractorMyJobComponent } from './components/site/contractor-my-job/contractor-my-job.component';
import { ContractorLayoutComponent } from './components/layout/contractor-layout/contractor-layout.component';
import { ContractorSidebarComponent } from './components/layout/contractor-sidebar/contractor-sidebar.component';
import { CompanySidebarComponent } from './components/layout/company-sidebar/company-sidebar.component';
import { CompanyLayoutComponent } from './components/layout/company-layout/company-layout.component';
import { ContractorJobComponent } from './components/site/contractor-job/contractor-job.component';
import { ContractorProfileComponent } from './components/site/contractor-profile/contractor-profile.component';
import { CompanyProfileComponent } from './components/site/company-profile/company-profile.component';
import { ContractorTimesheetsComponent } from './components/site/contractor-timesheets/contractor-timesheets.component';
import { CompanyMyJobComponent } from './components/site/company-my-job/company-my-job.component';
import { CompanyPostJobComponent } from './components/site/company-post-job/company-post-job.component';
import { CompanyTimesheetsComponent } from './components/site/company-timesheets/company-timesheets.component';
import { MessagesViewComponent } from './components/site/chat/messages-view/messages-view.component';
import { JobDetailsComponent } from './components/site/job-details/job-details.component';
import { ProfileComponent } from './components/site/profile/profile.component'
import { ContractorContactusComponent } from './components/site/contractor-contactus/contractor-contactus.component';
import { CompanyContactusComponent } from './components/site/company-contactus/company-contactus.component';
import { ContactsListComponent } from './components/site/chat/contacts-list/contacts-list.component';
import { ChatComponent } from './components/site/chat/chat/chat.component';
import { CompanyPlanComponent } from './components/site/company-plan/company-plan.component';
import { CompanyPaymentHistoryComponent } from './components/site/company-payment-history/company-payment-history.component';
import { VerifyComponent } from './components/site/verify/verify.component';



const config: SocketIoConfig = { url: environment.SOCKET_URL, options: {} };
const appRoutes: Routes = [
  {
    path: '',
    component: SiteLayoutComponent,
    children: [
      { path: '', component: HomeComponent },
      { path: 'home', component: HomeComponent },
      { path: 'company', component: CompanyComponent },
      { path: 'profile/:userId', component: ProfileComponent },
      { path: 'contact-us', component: ContactUsComponent },
      { path: 'contractor', component: ContractorComponent },
      { path: 'job-detail/:jobId', component: JobDetailsComponent },
      { path: 'privacy-policy', component: PrivacyPolicyComponent },
      { path: 'terms-condition', component: TermsConditionComponent },
      { path: 'verify/:token', component: VerifyComponent }
    ]
  },
  {
    path: '',
    component: ContentLayoutComponent,
    children: [
      { path: 'login', component: LoginComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'company-login', component: CompanyLoginComponent },
      { path: 'contractor-login', component: ContractorLoginComponent },
      { path: 'forgot-password', component: ForgotPasswordComponent }
    ]
  },
  {
    path: 'company',
    component: CompanyLayoutComponent,
    children: [
      { path: 'chat', component: ChatComponent },
      { path: 'faq', component: CompanyFaqComponent },
      { path: 'plan', component: CompanyPlanComponent },
      { path: 'profile', component: CompanyProfileComponent },
      { path: 'my-jobs', component: CompanyMyJobComponent },
      { path: 'post-job', component: CompanyPostJobComponent },
      { path: 'contact-us', component: CompanyContactusComponent },
      { path: 'timesheets', component: CompanyTimesheetsComponent },
      { path: 'payment-history', component: CompanyPaymentHistoryComponent }
    ]
  },
  {
    path: 'contractor',
    component: ContractorLayoutComponent,
    children: [
      { path: 'chat', component: ChatComponent },
      { path: 'faq', component: ContractorFaqComponent },
      { path: 'jobs', component: ContractorJobComponent },
      { path: 'profile', component: ContractorProfileComponent },
      { path: 'my-jobs', component: ContractorMyJobComponent },
      { path: 'contact-us', component: ContractorContactusComponent },
      { path: 'timesheets', component: ContractorTimesheetsComponent }
    ]
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  }];


@NgModule({
  declarations: [
    AppComponent,
    SiteHeaderComponent,
    SiteFooterComponent,
    SiteLayoutComponent,
    ContentLayoutComponent,
    HomeComponent,
    LoginComponent,
    PrivacyPolicyComponent,
    TermsConditionComponent,
    ForgotPasswordComponent,
    ContactUsComponent,
    ContractorLoginComponent,
    CompanyLoginComponent,
    CompanyComponent,
    ContractorComponent,
    RegisterComponent,
    CompanyFaqComponent,
    ContractorFaqComponent,
    ContractorMyJobComponent,
    ContractorLayoutComponent,
    ContractorSidebarComponent,
    CompanySidebarComponent,
    CompanyLayoutComponent,
    ContractorJobComponent,
    ContractorProfileComponent,
    CompanyProfileComponent,
    ContractorTimesheetsComponent,
    CompanyMyJobComponent,
    CompanyPostJobComponent,
    CompanyTimesheetsComponent,
    JobDetailsComponent,
    ProfileComponent,
    ContractorContactusComponent,
    CompanyContactusComponent,
    ChatComponent,
    ContactsListComponent,
    MessagesViewComponent,
    CompanyPlanComponent,
    CompanyPaymentHistoryComponent,
    VerifyComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    ToastrModule.forRoot(),
    HttpClientModule,
    BrowserAnimationsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    Ng2SearchPipeModule,
    NgxSpinnerModule,
    RouterModule.forRoot(appRoutes, { scrollPositionRestoration: 'enabled' }),
    ModalModule,
    TimepickerModule.forRoot(),
    GooglePlaceModule,
    AgmCoreModule.forRoot(),
    SocketIoModule.forRoot(config),
    AgmJsMarkerClustererModule,
    NgxStripeModule.forRoot(environment.STRIPE_PUBLISHABLE_KEY),
    PaginationModule.forRoot()
  ],
  providers: [
    GoogleMapsAPIWrapper,
    DatePipe,
    FormValidatorService,
    UserService,
    { provide: OWL_DATE_TIME_LOCALE, useValue: 'en-SG' },
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }


