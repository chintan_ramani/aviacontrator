module.exports = {
  packages: {

    '@agm/js-marker-clusterer':  {

      ignorableDeepImportMatchers: [

        /@agm\//

      ]

    },
    'ngx-toastr':  {

      ignorableDeepImportMatchers: [

        /ngx-toastr\//

      ]

    }

  }

};
